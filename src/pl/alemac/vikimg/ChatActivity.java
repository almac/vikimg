package pl.alemac.vikimg;

import java.util.Date;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.message.MessageItem;
import pl.alemac.vikimg.message.MessageListAdapter;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ChatActivity extends SherlockActivity {
	private ActionBar mActionBar;
	private EditText messageComposer;
	private Button sendButton;
	private ChatItem chatItem;
	private VikimgDatabaseHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		VikimgApp.chatActivity = this;
		VikimgApp.currentActivity = this;
		dbHelper = new VikimgDatabaseHelper(this);
		VikimgViewUtils.showRunningNotification(this);
		int chatItemPostion = Integer.parseInt(getIntent().getExtras().getString("position"));
		chatItem = VikimgApp.chatList.get(chatItemPostion);

		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle(chatItem.getParticipantName());

		ListView messageListView = (ListView) findViewById(R.id.message_list);
		messageListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		messageListView.setStackFromBottom(true);

		VikimgApp.messageListAdapter = new MessageListAdapter(this, R.layout.chat_message_row,
				chatItem.getMessageList());
		messageListView.setAdapter(VikimgApp.messageListAdapter);

		messageComposer = (EditText) findViewById(R.id.message_composer);
		messageComposer.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					onEnterPressed();
					return true;
				}
				return false;
			}
		});

		sendButton = (Button) findViewById(R.id.send_message);
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onEnterPressed();
			}
		});
	}

	private void onEnterPressed() {

		if (!TextUtils.isEmpty(messageComposer.getText()) && VikimgApp.connection.isConnected()
				&& VikimgApp.isUserAvailable) {
			chatItem.getMessageList().add(
					new MessageItem(messageComposer.getText().toString(), VikimgApp.user, new Date()));
			dbHelper.createMessage(VikimgDatabaseHelper.setMessageId(chatItem.getParticipantJid()), VikimgApp.user,
					messageComposer.getText().toString(), new Date());
			boolean success = true;
			try {
				chatItem.getChat().sendMessage(messageComposer.getText().toString());
			} catch (XMPPException e) {
				success = false;
				e.printStackTrace();
			}
			if (success) {
				messageComposer.setText(null);
				VikimgApp.messageListAdapter.notifyDataSetChanged();
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater minflater = getSupportMenuInflater();
		minflater.inflate(R.menu.chat, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				startActivity(new Intent(ChatActivity.this, MainScreen.class));
				return true;
			case R.id.back_buddy_list:
				VikimgApp.currentTab = 0;
				finish();
				startActivity(new Intent(ChatActivity.this, MainScreen.class));
				return true;
			case R.id.back_chat_list:
				VikimgApp.currentTab = 1;
				finish();
				startActivity(new Intent(ChatActivity.this, MainScreen.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(ChatActivity.this, MainScreen.class));
			finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		VikimgApp.chatActivity = null;
		VikimgApp.messageListAdapter = null;
		if (chatItem.isNewMessage()) {
			chatItem.setNewMessage(false);
			VikimgViewUtils.cancelNotification(0, this);
			refreshChatList();
		}
	}

	private void refreshChatList() {
		VikimgApp.buddyListActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				VikimgApp.chatListAdapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

}
