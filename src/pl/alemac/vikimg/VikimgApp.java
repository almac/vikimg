package pl.alemac.vikimg;

import java.util.ArrayList;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPConnection;

import pl.alemac.vikimg.buddy.BuddyGroup;
import pl.alemac.vikimg.buddyList.ExpandableBuddyListAdapter;
import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.chatList.ChatListAdapter;
import pl.alemac.vikimg.message.MessageItem;
import pl.alemac.vikimg.message.MessageListAdapter;
import pl.alemac.vikimg.xmpp.listeners.BuddyListener;
import pl.alemac.vikimg.xmpp.listeners.VikimgChatManagerListener;
import pl.alemac.vikimg.xmpp.listeners.VikimgConnectionListener;
import pl.alemac.vikimg.xmpp.listeners.VikimgMessageListener;
import android.app.Activity;
import android.app.Application;
import android.media.AudioManager;

public class VikimgApp extends Application {

	public static String STATUS_PREF = "STATUS_PREFERENCES";
	public static String SETTINGS_PREF = "SETTINGS_PREFERENCES";
	public static String user = null;

	public static ArrayList<BuddyGroup> buddyList = new ArrayList<BuddyGroup>();
	public static ArrayList<ChatItem> chatList = new ArrayList<ChatItem>();
	public static ArrayList<MessageItem> archiveMessageList = new ArrayList<MessageItem>();
	public static XMPPConnection connection = null;

	public static ChatListAdapter chatListAdapter = null;
	public static ExpandableBuddyListAdapter buddyListAdapter = null;
	public static MessageListAdapter messageListAdapter = null;

	public static Roster roster = null;

	public static BuddyListener rosterListener = null;
	public static VikimgMessageListener messageListener = null;
	public static VikimgChatManagerListener chatManagerListener = null;
	public static VikimgConnectionListener connectionListener;
	
	public static Activity buddyListActivity = null;
	public static Activity chatActivity = null;
	public static Activity currentActivity = null;
	
	public static AudioManager audioManager = null;

	public static int currentTab = 0;
	
	public static boolean inBackground = false;
	public static boolean isUserAvailable = true;
	public static boolean isSound;
	public static boolean isVibrate;
	

}
