package pl.alemac.vikimg.buddyList;

import java.util.ArrayList;

import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

import pl.alemac.vikimg.MainScreen;
import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class NewBuddyActivity extends SherlockActivity {

	private EditText buddyName;
	private EditText buddyJid;
	private AutoCompleteTextView buddyGroup;
	private Button okButton;
	private Button cancelButton;
	private ActionBar mActionBar;
	private String buddyNewName;
	private String buddyNewJid;
	private String buddyNewGroup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_buddy);
		VikimgApp.currentActivity = this;
		VikimgViewUtils.showRunningNotification(this);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle("Nowy kontakt");

		buddyName = (EditText) findViewById(R.id.new_name);
		buddyJid = (EditText) findViewById(R.id.new_jid);
		buddyGroup = (AutoCompleteTextView) findViewById(R.id.new_group);
		okButton = (Button) findViewById(R.id.new_ok);
		cancelButton = (Button) findViewById(R.id.new_cancel);

		ArrayList<String> groups = new ArrayList<String>();
		for (RosterGroup group : VikimgApp.roster.getGroups()) {
			groups.add(group.getName());
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,
				groups);

		buddyGroup.setAdapter(adapter);

		okButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onNewButton();
			}
		});

		cancelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
				startActivity(new Intent(NewBuddyActivity.this, MainScreen.class));
			}
		});
	}

	private void onNewButton() {
		buddyNewName = buddyName.getText().toString();
		buddyNewJid = buddyJid.getText().toString();
		buddyNewGroup = buddyGroup.getText().toString();

		boolean cancel = false;
		View focusView = null;

		buddyJid.setError(null);
		buddyName.setError(null);
		buddyGroup.setError(null);

		if (TextUtils.isEmpty(buddyNewJid)) {
			buddyJid.setError("Brak JID");
			focusView = buddyJid;
			cancel = true;
		}
		if (!buddyNewJid.contains("@")) {
			buddyJid.setError("Zly JID");
			focusView = buddyJid;
			cancel = true;
		}
		if (TextUtils.isEmpty(buddyNewName)) {
			buddyName.setError("Brak nicka");
			focusView = buddyName;
			cancel = true;
		}
		if (TextUtils.isEmpty(buddyNewGroup)) {
			buddyGroup.setError("Brak grupy");
			focusView = buddyGroup;
			cancel = true;
		}

		if (cancel) {
			focusView.requestFocus();

		} else {
			new CreateBuddyTask().execute();
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		startActivity(new Intent(NewBuddyActivity.this, MainScreen.class));
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(NewBuddyActivity.this, MainScreen.class));
			finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

	private class CreateBuddyTask extends AsyncTask<String, Void, String> {

		private ProgressDialog dialog;

		public CreateBuddyTask() {
			dialog = new ProgressDialog(NewBuddyActivity.this);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getResources().getString(R.string.register_progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "0";

			String group[] = { buddyNewGroup };
			if (VikimgApp.connection.isConnected()) {
				try {
					Presence subscribe = new Presence(Presence.Type.subscribe);
					subscribe.setTo(buddyNewJid);
					VikimgApp.connection.sendPacket(subscribe);
					VikimgApp.roster.createEntry(buddyNewJid, buddyNewName, group);
				} catch (XMPPException e) {
					result = "1";
					e.printStackTrace();
				}
			} else {
				result = "2";
			}
			return result;
		}

		@Override
		protected void onPostExecute(String results) {

			int result = Integer.parseInt(results);
			if (result == 0) {
				dialog.dismiss();
				startActivity(new Intent(NewBuddyActivity.this, MainScreen.class));
				finish();
			} else if (result == 1) {
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(NewBuddyActivity.this, "Serwer kontaktu nie odpowiada");
			} else if (result == 2) {
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(NewBuddyActivity.this, "Brak polaczenia z serwerem");
			}

		}
	}

}
