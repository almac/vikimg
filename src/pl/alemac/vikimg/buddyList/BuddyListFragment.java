package pl.alemac.vikimg.buddyList;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPException;

import pl.alemac.vikimg.ChatActivity;
import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.buddy.BuddyEntry;
import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.ExpandableListContextMenuInfo;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

public class BuddyListFragment extends SherlockFragment {
	private ExpandableListView buddyListView;
	private VikimgDatabaseHelper dbHelper;

	private final static int BUDDY_TALK = 1;
	private final static int BUDDY_EDIT = 2;
	private final static int BUDDY_REMOVE = 3;
	private final static int BUDDY_CONTEXT_MENU = 0;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_buddy_list, container, false);
		return view;
	}

	public void onStart() {
		super.onStart();
		dbHelper = new VikimgDatabaseHelper(getActivity());
		VikimgApp.buddyListAdapter = new ExpandableBuddyListAdapter(getActivity(), VikimgApp.buddyList);
		buddyListView = (ExpandableListView) getActivity().findViewById(R.id.buddy_list);
		buddyListView.setAdapter(VikimgApp.buddyListAdapter);
		registerForContextMenu(buddyListView);
		setListeners();

	}

	private void setListeners() {

		buddyListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				createChatWithBuddy(groupPosition, childPosition);
				return false;
			}
		});

	}

	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
		int type = ExpandableListView.getPackedPositionType(info.packedPosition);
		int group = ExpandableListView.getPackedPositionGroup(info.packedPosition);
		int child = ExpandableListView.getPackedPositionChild(info.packedPosition);

		if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
			menu.setHeaderTitle(VikimgApp.buddyList.get(group).getBuddyList().get(child).getName());
			menu.add(BUDDY_CONTEXT_MENU, BUDDY_TALK, 0, "Rozmowa");
			menu.add(BUDDY_CONTEXT_MENU, BUDDY_EDIT, 0, "Edycja");
			menu.add(BUDDY_CONTEXT_MENU, BUDDY_REMOVE, 0, "Usun");
		}

	}

	public boolean onContextItemSelected(MenuItem menuItem) {

		if (menuItem.getGroupId() == BUDDY_CONTEXT_MENU) {
			ExpandableListContextMenuInfo info = (ExpandableListContextMenuInfo) menuItem.getMenuInfo();
			int groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
			int childPos = ExpandableListView.getPackedPositionChild(info.packedPosition);

			switch (menuItem.getItemId()) {

				case BUDDY_TALK:
					createChatWithBuddy(groupPos, childPos);
					return true;

				case BUDDY_EDIT:
					Intent intent2 = new Intent(getActivity(), BuddyEditActivity.class);
					intent2.putExtra("buddyEntry", VikimgApp.buddyList.get(groupPos).getBuddyList().get(childPos));
					intent2.putExtra("groupName", VikimgApp.buddyList.get(groupPos).getName());
					startActivity(intent2);
					getActivity().finish();
					return true;

				case BUDDY_REMOVE:
					Log.i("Menu", "Usun kontakt");
					createBuddyRemoveDialog(groupPos, childPos);
					return true;

				default:
					return super.onContextItemSelected(menuItem);

			}
		}
		return false;

	}

	private void createBuddyRemoveDialog(final int groupPos, final int childPos) {
		if (VikimgApp.connection.isConnected()) {
			final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());

			b.setMessage("Czy chcesz usunac kontakt?");
			b.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					new RemoveBuddyTask(groupPos, childPos).execute();
				}
			});
			b.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					Log.i("Removal", "No clicked, on: " + childPos);
				}
			});

			b.show();
		} else {
			Toast.makeText(getActivity(), "Brak polaczenia z serwerem", Toast.LENGTH_SHORT).show();
		}

	}

	private void createChatWithBuddy(int groupPosition, int childPosition) {
		int chatInfoPosition = 0;
		int loopIndex = 0;
		boolean chatExists = false;
		String buddyJid = VikimgApp.buddyList.get(groupPosition).getBuddyList().get(childPosition).getJid();
		for (ChatItem item : VikimgApp.chatList) {
			if (item.getParticipantJid().equals(buddyJid)) {
				chatExists = true;
				chatInfoPosition = loopIndex;
			}
			loopIndex++;
		}
		if (!chatExists) {
			BuddyEntry buddy = VikimgApp.buddyList.get(groupPosition).getBuddyList().get(childPosition);
			VikimgApp.chatList.add(new ChatItem(VikimgApp.user, buddy.getJid(), buddy.getName()));
			dbHelper.createArchiveBuddy(buddy.getJid(), buddy.getName());
			chatInfoPosition = VikimgApp.chatList.size() - 1;
		}
		Intent intent = new Intent(getActivity(), ChatActivity.class);
		intent.putExtra("position", Integer.toString(chatInfoPosition));
		startActivity(intent);
		getActivity().finish();

	}

	private class RemoveBuddyTask extends AsyncTask<String, Void, String> {

		private ProgressDialog dialog;
		private int groupPos;
		private int childPos;

		public RemoveBuddyTask(int group, int buddy) {
			dialog = new ProgressDialog(getActivity());
			groupPos = group;
			childPos = buddy;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getResources().getString(R.string.register_progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "0";
			String removedJid = VikimgApp.buddyList.get(groupPos).getBuddyList().get(childPos).getJid();
			RosterEntry entry = VikimgApp.roster.getEntry(removedJid);
			try {
				VikimgApp.roster.removeEntry(entry);
			} catch (XMPPException e) {
				result = "1";
				e.printStackTrace();
			}
			return result;

		}

		@Override
		protected void onPostExecute(String results) {

			int result = Integer.parseInt(results);
			if (result == 0) {
				dialog.dismiss();
			} else if (result == 1) {
				dialog.dismiss();
				Toast.makeText(getActivity(), "Blad serwera", Toast.LENGTH_SHORT).show();
			}

		}
	}
}
