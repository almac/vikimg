package pl.alemac.vikimg.buddyList;

import java.util.ArrayList;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPException;

import pl.alemac.vikimg.MainScreen;
import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.buddy.BuddyEntry;
import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.message.MessageItem;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class BuddyEditActivity extends SherlockActivity {

	private EditText buddyName;
	private TextView buddyJid;
	private AutoCompleteTextView buddyGroup;
	private Button editButton;
	private Button cancelButton;
	private BuddyEntry entry;
	private String oldGroup;
	private ActionBar mActionBar;
	private VikimgDatabaseHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.buddy_edit_activity);

		VikimgApp.currentActivity = this;
		dbHelper = new VikimgDatabaseHelper(this);
		VikimgViewUtils.showRunningNotification(this);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		entry = (BuddyEntry) getIntent().getSerializableExtra("buddyEntry");
		mActionBar.setTitle("Edycja kontaktu: " + entry.getName());

		oldGroup = getIntent().getStringExtra("groupName");
		buddyGroup = (AutoCompleteTextView) findViewById(R.id.edit_group);
		buddyName = (EditText) findViewById(R.id.edit_name);
		buddyJid = (TextView) findViewById(R.id.edit_jid);
		editButton = (Button) findViewById(R.id.edit_ok);
		cancelButton = (Button) findViewById(R.id.edit_cancel);

		ArrayList<String> groups = new ArrayList<String>();
		for (RosterGroup group : VikimgApp.roster.getGroups()) {
			groups.add(group.getName());
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,
				groups);

		buddyGroup.setAdapter(adapter);

		buddyName.setText(entry.getName());
		buddyJid.setText(entry.getJid());
		buddyGroup.setText(oldGroup);

		editButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onEditButton();

			}
		});

		cancelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(BuddyEditActivity.this, MainScreen.class));
				finish();
			}
		});
	}

	private void onEditButton() {
		String newGroup = buddyGroup.getText().toString();
		String newName = buddyName.getText().toString();

		buddyName.setError(null);
		buddyGroup.setError(null);
		boolean cancel = false;
		View focusView = null;

		if (TextUtils.isEmpty(newName)) {
			buddyName.setError("Brak wpisanego nick'a");
			focusView = buddyName;
			cancel = true;
		}
		if (TextUtils.isEmpty(newGroup)) {
			buddyGroup.setError("Brak przypisanej grupy");
			focusView = buddyGroup;
			cancel = true;
		}
		if (cancel) {
			focusView.requestFocus();

		} else {
			new EditBuddyTask(newName, newGroup).execute();
		}

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		startActivity(new Intent(BuddyEditActivity.this, MainScreen.class));
		finish();
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(BuddyEditActivity.this, MainScreen.class));
			finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

	private class EditBuddyTask extends AsyncTask<String, Void, String> {

		private ProgressDialog dialog;
		private String result;
		private String newName;
		private String newGroup;

		public EditBuddyTask(String name, String group) {
			dialog = new ProgressDialog(BuddyEditActivity.this);
			result = "5";
			newName = name;
			newGroup = group;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getResources().getString(R.string.register_progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			if (VikimgApp.connection.isConnected()) {
				boolean cancel = false;
				RosterEntry rEntry = VikimgApp.roster.getEntry(entry.getJid());
				rEntry.setName(newName);

				if (!oldGroup.equals(newGroup)) {
					RosterGroup rOldGroup = VikimgApp.roster.getGroup(oldGroup);
					RosterGroup rNewGroup = null;
					for (RosterGroup group : VikimgApp.roster.getGroups()) {
						if (group.getName().equals(newGroup)) {
							rNewGroup = group;
						}
					}

					if (rNewGroup == null) {
						rNewGroup = VikimgApp.roster.createGroup(newGroup);
					}
					try {
						rOldGroup.removeEntry(rEntry);
					} catch (XMPPException e) {
						result = "3";
						cancel = true;
						e.printStackTrace();
					}

					try {
						rNewGroup.addEntry(rEntry);
					} catch (XMPPException e) {
						result = "4";
						cancel = true;
						e.printStackTrace();
					}

				}// else {
				//	rEntry.setName(newName);
				//}

				if (!cancel) {
					dbHelper.updateArchiveBuddy(newName, entry.getJid());
					for (ChatItem item : VikimgApp.chatList) {
						if (item.getParticipantJid().equals(entry.getJid())) {
							item.setParticipantName(newName);
							for (MessageItem message : item.getMessageList()) {
								if (message.getSender().equals(entry.getName())) {
									message.setSender(newName);
								}
							}
						}
					}
					result = "0";

				}
			} else {
				return "2";
			}
			return result;

			// 0 - sukces
			// 1 - nic error wpisania
			// 2 - brak polaczenia
			// 3 - bald przy zmianie kontaktu
			// 4 - serwer nie odpowaida
		}

		@Override
		protected void onPostExecute(String result) {

			int res = Integer.parseInt(result);

			switch (res) {
				case 0:
					dialog.dismiss();
					startActivity(new Intent(BuddyEditActivity.this, MainScreen.class));
					finish();
					break;
				case 1:
					dialog.dismiss();
					break;
				case 2:
					dialog.dismiss();
					Toast.makeText(BuddyEditActivity.this, "Brak polaczenia z serwerem", Toast.LENGTH_SHORT).show();
					break;
				case 3:
					dialog.dismiss();
					VikimgViewUtils.showErrorDialog(BuddyEditActivity.this, "Blad przy zmianie kontaktu");
					break;
				case 4:
					dialog.dismiss();
					VikimgViewUtils.showErrorDialog(BuddyEditActivity.this, "Serwer kontaktu nie odpowiada");
					break;
				case 5:
					dialog.dismiss();
					VikimgViewUtils.showErrorDialog(BuddyEditActivity.this, "Nieznany blad");
					break;
			}

		}
	}
}
