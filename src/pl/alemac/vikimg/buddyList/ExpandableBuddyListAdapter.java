package pl.alemac.vikimg.buddyList;

import java.util.ArrayList;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.buddy.BuddyEntry;
import pl.alemac.vikimg.buddy.BuddyGroup;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableBuddyListAdapter extends BaseExpandableListAdapter {

	private LayoutInflater inflater;
	private ArrayList<BuddyGroup> buddyGroupList;

	public ExpandableBuddyListAdapter(Context context, ArrayList<BuddyGroup> buddyGroupList) {
		this.buddyGroupList = buddyGroupList;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return buddyGroupList.get(groupPosition).getBuddyList().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.buddy_list_child, parent, false);
		}

		TextView buddyName = (TextView) convertView.findViewById(R.id.buddy_name);
		TextView buddyDescription = (TextView) convertView.findViewById(R.id.buddy_description);
		ImageView buddyStatus = (ImageView) convertView.findViewById(R.id.buddy_status_image);

		BuddyEntry entry = buddyGroupList.get(groupPosition).getBuddyList().get(childPosition);

		buddyName.setText(entry.getName());
		buddyDescription.setText(entry.getDescription());
		buddyStatus.setImageResource(entry.getStatus().getResource());

		convertView.setTag(entry.getJid());

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return buddyGroupList.get(groupPosition).getBuddyList().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return buddyGroupList.get(groupPosition).getName();
	}

	@Override
	public int getGroupCount() {
		return buddyGroupList.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.buddy_list_parent, parent, false);
		}

		TextView groupName = (TextView) convertView.findViewById(R.id.buddy_group_name);
		TextView groupOnlineRatio = (TextView) convertView.findViewById(R.id.online_ratio);
		String group = buddyGroupList.get(groupPosition).getName();
		groupName.setText(group);
		groupOnlineRatio.setText(buddyGroupList.get(groupPosition).getOnlineRatio());
		convertView.setTag(group);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		super.registerDataSetObserver(observer);
	}

}
