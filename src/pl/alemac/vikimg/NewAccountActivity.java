package pl.alemac.vikimg;

import java.util.HashMap;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.database.model.Account;
import pl.alemac.vikimg.database.model.XmppAccount;
import pl.alemac.vikimg.utilities.VikimgCryptoUtils;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class NewAccountActivity extends SherlockActivity {

	private ActionBar mActionBar;
	private EditText newAccountNickView;
	private EditText newAccountPwdView;
	private EditText newXmppAccountJidView;
	private EditText newXmppAccountPwdView;
	private CheckBox checkboxNewXmmpAccount;
	private Activity act;

	private Account account;
	private XmppAccount xmppAccount;
	private VikimgDatabaseHelper dbHelper;

	private String newAccountNick;
	private String newAccountPwd;
	private String newXmppAccountJid;
	private String newXmppAccountPwd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		act = this;
		dbHelper = new VikimgDatabaseHelper(this);

		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle("Nowe konto");
		setContentView(R.layout.activity_new_account);

		newAccountNickView = (EditText) findViewById(R.id.new_account_nick);
		newAccountPwdView = (EditText) findViewById(R.id.new_account_pwd);
		newXmppAccountJidView = (EditText) findViewById(R.id.new_xmpp_account_jid);
		newXmppAccountPwdView = (EditText) findViewById(R.id.new_xmpp_account_pwd);

		
		newAccountPwdView.setHint(getResources().getString(R.string.prompt_password));
		newXmppAccountPwdView.setHint(getResources().getString(R.string.prompt_password));

		newAccountNick = new String();
		newAccountPwd = new String();
		newXmppAccountJid = new String();
		newXmppAccountPwd = new String();

		findViewById(R.id.button_create_account).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptCreateNewAccount();
			}
		});

		findViewById(R.id.button_dont_create_account).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(NewAccountActivity.this, LoginActivity.class));
				finish();
			}
		});

		checkboxNewXmmpAccount = (CheckBox) findViewById(R.id.new_xmpp_account);

	}

	private void attemptCreateNewAccount() {

		newAccountNickView.setError(null);
		newAccountPwdView.setError(null);
		newXmppAccountJidView.setError(null);
		newXmppAccountPwdView.setError(null);

		boolean accountExists = true;

		newAccountNick = newAccountNickView.getText().toString();
		newAccountPwd = newAccountPwdView.getText().toString();
		newXmppAccountJid = newXmppAccountJidView.getText().toString();
		newXmppAccountPwd = newXmppAccountPwdView.getText().toString();

		account = dbHelper.getAccountByNick(newAccountNick);
		if (account == null) {
			accountExists = false;
		}

		dbHelper.closeDB();

		boolean cancel = false;
		View focusView = null;

		if (TextUtils.isEmpty(newAccountNick)) {
			newAccountNickView.setError(getString(R.string.error_field_required));
			focusView = newAccountNickView;
			cancel = true;
		} else if (accountExists) {
			newAccountNickView.setError(getString(R.string.error_field_doubled));
			focusView = newAccountNickView;
			cancel = true;
		}

		if (TextUtils.isEmpty(newAccountPwd)) {
			newAccountPwdView.setError(getString(R.string.error_field_required));
			focusView = newAccountPwdView;
			cancel = true;
		} else if (newAccountPwd.length() < 5) {
			newAccountPwdView.setError(getString(R.string.error_invalid_password));
			focusView = newAccountPwdView;
			cancel = true;
		}

		if (TextUtils.isEmpty(newXmppAccountJid)) {
			newXmppAccountJidView.setError(getString(R.string.error_field_required));
			focusView = newXmppAccountJidView;
			cancel = true;
		} else if (!newXmppAccountJid.contains("@")) {
			newXmppAccountJidView.setError(getString(R.string.error_invalid_jid));
			focusView = newXmppAccountJidView;
			cancel = true;
		}

		if (TextUtils.isEmpty(newXmppAccountPwd)) {
			newXmppAccountPwdView.setError(getString(R.string.error_field_required));
			focusView = newXmppAccountPwdView;
			cancel = true;
		}

		if (cancel) {
			focusView.requestFocus();
		} else {
			createAccount();
		}

	}

	private void createAccount() {

		String test = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

		String accountSalt = VikimgCryptoUtils.getSalt();
		String md5hash = VikimgCryptoUtils.getPassword(newAccountPwd, accountSalt);
		String encryptedPwd = null;
		try {
			encryptedPwd = VikimgCryptoUtils.encrypt(test, newXmppAccountPwd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// setting accounts
		account = new Account();
		xmppAccount = new XmppAccount();

		account.setNick(newAccountNick);
		account.setPassword(accountSalt + md5hash);

		xmppAccount.setJid(newXmppAccountJid);
		xmppAccount.setPassword(encryptedPwd);

		new XmppRegisterTask(newXmppAccountJid, newXmppAccountPwd, checkboxNewXmmpAccount.isChecked()).execute();

	}

	private class XmppRegisterTask extends AsyncTask<String, String, String> {

		private ProgressDialog dialog;
		private String loginData;
		private String pwd;
		private boolean newAccount;

		public XmppRegisterTask(String data, String password, boolean newAccount) {
			dialog = new ProgressDialog(NewAccountActivity.this);
			loginData = data;
			pwd = password;
			this.newAccount = newAccount;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getResources().getString(R.string.register_progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			if (newAccount) {
				return createNewXmppAccount(loginData, pwd);
			} else {
				return createExistingXmppAccount();
			}

		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);
			dialog.dismiss();
			int result = Integer.parseInt(results);

			if (result == 0) {
				Log.d("ccc", "Rejestracja zakonczona pomyslnie");
				dialog.dismiss();
				
				final AlertDialog.Builder b = new AlertDialog.Builder(NewAccountActivity.this);
				b.setMessage(R.string.register_success);
				b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						startActivity(new Intent(NewAccountActivity.this, LoginActivity.class));
						
					}
				});

				b.show();
				
			} else if (result == 1) {
				Log.d("ccc", "Blad polaczenia");
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(act, getResources().getString(R.string.no_connection));
			} else if (result == 2) {
				Log.d("ccc", getResources().getString(R.string.register_failed));
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(act, getResources().getString(R.string.register_failed));
			} else if (result == 3) {
				Log.d("ccc", "Serwer nie pozwala na rejestracje");
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(act, getResources().getString(R.string.register_forbidden));
			}

		}

	}

	private String createExistingXmppAccount() {
		try {
			dbHelper.createAccount(account, xmppAccount);
			dbHelper.createAccountArchive(account.getNick());
			dbHelper.closeDB();
			return "0";
		} catch (Exception e) {
			e.printStackTrace();
			return "1";
		}
	}

	private String createNewXmppAccount(String loginData, String pwd) {

		String result = null;
		boolean accountCreated = true;
		String[] data = loginData.split("@");
		String login = data[0];
		String server = data[1];
		Log.d("ccc", login);
		Log.d("ccc", server);
		ConnectionConfiguration config = new ConnectionConfiguration(server, 5222);
		Connection con = new XMPPConnection(config);
		config.setSASLAuthenticationEnabled(true);

		try {
			con.connect();
		} catch (XMPPException e) {
			e.printStackTrace();
			result = "1";
			accountCreated = false;
			return result;
		}
		if (con.isConnected()) {

			AccountManager mgr = new AccountManager(con);
			if (mgr.supportsAccountCreation()) {
				try {

					HashMap<String, String> attr = new HashMap<String, String>();
					attr.put("username", login);
					attr.put("password", pwd);

					mgr.createAccount(login, pwd, attr);
				} catch (XMPPException e) {
					e.printStackTrace();
					result = "2";
					con.disconnect();
					accountCreated = false;
					return result;
				}

				if (accountCreated == true) {
					result = "0";
					dbHelper.createAccount(account, xmppAccount);
					dbHelper.createAccountArchive(account.getNick());
					dbHelper.closeDB();
					con.disconnect();
					return result;
				}
			} else {
				result = "3";
				con.disconnect();
				return result;
			}
		}
		return result;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		startActivity(new Intent(NewAccountActivity.this, LoginActivity.class));
		finish();
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(NewAccountActivity.this, LoginActivity.class));
			finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

}
