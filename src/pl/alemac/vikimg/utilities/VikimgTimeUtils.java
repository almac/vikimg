package pl.alemac.vikimg.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VikimgTimeUtils {

	private static SimpleDateFormat dateRetrival = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat timeRetrieval = new SimpleDateFormat("HH:mm:ss");
	private static SimpleDateFormat fullDateRetrival = new SimpleDateFormat("dd/MM/yyyyHH:mm:ss");

	public static String getTime(Date date) {
		return timeRetrieval.format(date);
	}

	public static String getDate(Date date) {
		return dateRetrival.format(date);
	}
	public static String getFullDate(Date date) {
		return fullDateRetrival.format(date);
	}
	
	public static Date getFullDate(String string) {
		try {
			return fullDateRetrival.parse(string);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
