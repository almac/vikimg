package pl.alemac.vikimg.utilities;

import pl.alemac.vikimg.buddyList.BuddyListFragment;
import pl.alemac.vikimg.chatList.ChatListFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class vikIMgFragmentPagerAdapter extends FragmentStatePagerAdapter {

	final int PAGE_COUNT = 2;
	
	public vikIMgFragmentPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {
	    Bundle data = new Bundle();
        switch(arg0){
            case 0:
                BuddyListFragment contactFragment = new BuddyListFragment();
                data.putInt("current_page", arg0+1);
                contactFragment.setArguments(data);
                return contactFragment;
                
            case 1:
                ChatListFragment chatListFragment = new ChatListFragment();
                data.putInt("current_page", arg0+1);
                chatListFragment.setArguments(data);
                return chatListFragment;
        }
        return null;
	}

	@Override
	public int getCount() {
		return PAGE_COUNT;
	}

}
