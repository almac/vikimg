package pl.alemac.vikimg.utilities;

import java.util.List;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

public class VikimgViewUtils {

	public static void setInBackground(Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				Log.d("pause", "background");
				VikimgApp.inBackground = true;
			} else {
				Log.d("pause", "ciagle apka");
				VikimgApp.inBackground = false;
			}
		}
	}

	public static void showErrorDialog(Context context, String errorCode) {
		final AlertDialog.Builder b = new AlertDialog.Builder(context);
		b.setMessage(errorCode);
		b.setNegativeButton("Ok", null);
		b.show();

	}

	public static void showNotification() {

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(VikimgApp.buddyListActivity)
				.setSmallIcon(R.drawable.new_message).setContentTitle("vikIMg").setContentText("Masz nowa wiadomosc!")
				.setAutoCancel(true);
		Intent resultIntent = new Intent(VikimgApp.buddyListActivity, VikimgApp.currentActivity.getClass());
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(VikimgApp.buddyListActivity);
//		stackBuilder.addParentStack(MainScreen.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) VikimgApp.buddyListActivity
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, mBuilder.build());

	}

	public static void cancelNotification(int number, Activity activity) {
		NotificationManager mNotificationManager = (NotificationManager) VikimgApp.buddyListActivity
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(number);
	}

	public static void showRunningNotification(Activity activity) {
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(activity)
				.setSmallIcon(R.drawable.ic_launcher).setContentTitle("vikIMg jest uruchomiony").setOngoing(true)
				.setContentText("Polaczony do " + VikimgApp.user).setAutoCancel(false);
		Intent resultIntent = new Intent(activity, VikimgApp.currentActivity.getClass());
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(activity);
		stackBuilder.addParentStack(VikimgApp.currentActivity.getClass());
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT);//PendingIntent.FLAG_UPDATE_CURRENT)
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) activity
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(1, mBuilder.build());

	}
}
