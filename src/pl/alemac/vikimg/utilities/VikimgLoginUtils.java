package pl.alemac.vikimg.utilities;

import java.util.Collections;

import org.jivesoftware.smack.AndroidConnectionConfiguration;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.Roster.SubscriptionMode;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Type;

import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.buddy.BuddyEntry;
import pl.alemac.vikimg.buddy.BuddyEntryComparator;
import pl.alemac.vikimg.buddy.BuddyGroup;
import pl.alemac.vikimg.buddy.BuddyGroupComparator;
import pl.alemac.vikimg.buddy.BuddyStatus;
import pl.alemac.vikimg.buddy.BuddyStatusComparator;
import pl.alemac.vikimg.xmpp.listeners.BuddyListener;
import pl.alemac.vikimg.xmpp.listeners.OfflinePacketListener;
import pl.alemac.vikimg.xmpp.listeners.VikimgChatManagerListener;
import pl.alemac.vikimg.xmpp.listeners.VikimgConnectionListener;
import pl.alemac.vikimg.xmpp.listeners.VikimgMessageListener;
import android.app.Activity;
import android.content.SharedPreferences;

public class VikimgLoginUtils {

	public static String setUpConnection(Activity activity, String jid, String xmppPwd) {

		String data[] = jid.split("@");
		String username = data[0];
		if (data[1].equals("gmail.com")) {
			username = jid;
		}
		
		SmackAndroid.init(activity.getApplicationContext());
		SmackConfiguration.setPacketReplyTimeout(10000);
		AndroidConnectionConfiguration connConfig;
		
		VikimgApp.connection = new XMPPConnection("null");
		
		try {
			connConfig = new AndroidConnectionConfiguration(data[1], 5222);
			connConfig.setSASLAuthenticationEnabled(true);
			connConfig.setReconnectionAllowed(true);
			SASLAuthentication.registerSASLMechanism("DIGEST-MD5", MySASLDigestMD5Mechanism.class);
			SASLAuthentication.supportSASLMechanism("PLAIN");

			// connConfig.setCompressionEnabled(true);
			VikimgApp.connection = new XMPPConnection(connConfig);
		} catch (XMPPException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return "1";
		}
		PacketFilter filter = new PacketTypeFilter(Message.class);
		OfflinePacketListener packetListener = new OfflinePacketListener();

		try {
			VikimgApp.connection.connect();
		} catch (XMPPException ex) {
			ex.printStackTrace();
			return "1";
		}

		try {
			VikimgApp.connection.login(username, xmppPwd, "vikIMg");
			VikimgApp.isUserAvailable = true;
			setPresence(activity);

		} catch (XMPPException ex) {
			ex.printStackTrace();
			return "2";
		}

		VikimgApp.roster = VikimgApp.connection.getRoster();
		VikimgApp.connection.addPacketListener(packetListener, filter);
		VikimgApp.rosterListener = new BuddyListener();

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		VikimgApp.roster.setSubscriptionMode(SubscriptionMode.accept_all);
		VikimgApp.roster.addRosterListener(VikimgApp.rosterListener);
		VikimgApp.messageListener = new VikimgMessageListener();
		VikimgApp.connectionListener = new VikimgConnectionListener();
		VikimgApp.connection.addConnectionListener(VikimgApp.connectionListener);

		VikimgApp.chatManagerListener = new VikimgChatManagerListener();
		ChatManager chatManager = VikimgApp.connection.getChatManager();
		chatManager.addChatListener(VikimgApp.chatManagerListener);

		populateList();

		return "0";
	}

	private static void setPresence(Activity activity) {
		Presence presence = new Presence(Presence.Type.available);
		SharedPreferences settings = activity.getSharedPreferences(VikimgApp.user + "_" + VikimgApp.STATUS_PREF, 0);
		String description = settings.getString("description", "default");
		int status = settings.getInt("status", 1);
		if (!description.equals("default")) {
			presence.setStatus(description);
		}
		if (status == 5) {
			VikimgApp.isUserAvailable = false;
			presence.setType(Type.unavailable);
		} else {
			VikimgApp.isUserAvailable = true;
			presence.setMode(BuddyStatus.retrievePresence(BuddyStatus.values()[status]));
		}
		VikimgApp.connection.sendPacket(presence);
	}

	private static void populateList() {

		for (RosterEntry entry : VikimgApp.roster.getUnfiledEntries()) {
			RosterGroup bonusGrp = null;

			if (VikimgApp.roster.getGroup("Nieuporzadkowane") == null) {
				bonusGrp = VikimgApp.roster.createGroup("Nieuporzadkowane");
			} else {
				bonusGrp = VikimgApp.roster.getGroup("Nieuporzadkowane");
			}

			try {
				bonusGrp.addEntry(entry);
			} catch (XMPPException e) {
				e.printStackTrace();
			}
		}

		int i = 0;

		for (RosterGroup group : VikimgApp.roster.getGroups()) {
			VikimgApp.buddyList.add(new BuddyGroup(group.getName()));
			for (RosterEntry entry : group.getEntries()) {
				VikimgApp.buddyList
						.get(i)
						.getBuddyList()
						.add(new BuddyEntry(entry.getName(), entry.getUser(), VikimgApp.roster.getPresence(
								entry.getUser()).getStatus(), BuddyStatus.retrieveStatus(VikimgApp.roster
								.getPresence(entry.getUser()))));
			}
			i++;
		}
		if (VikimgApp.isUserAvailable) {
			setOnlineBuddyRatio();
		} else {
			setOfflineBuddyList();
		}

		Collections.sort(VikimgApp.buddyList, new BuddyGroupComparator());
		for (BuddyGroup group : VikimgApp.buddyList) {
			Collections.sort(group.getBuddyList(), new BuddyEntryComparator());
			Collections.sort(group.getBuddyList(), new BuddyStatusComparator());
		}
	}

	private static void setOnlineBuddyRatio() {
		for (BuddyGroup group : VikimgApp.buddyList) {
			RosterGroup rGroup = VikimgApp.roster.getGroup(group.getName());
			int online = 0;
			for (RosterEntry entry : rGroup.getEntries()) {
				if (VikimgApp.roster.getPresence(entry.getUser()).isAvailable()) {
					online++;
				}
				group.setOnlineRatio("(" + online + "/" + group.getBuddyList().size() + ")");
			}
		}
	}

	private static void setOfflineBuddyList() {
		for (BuddyGroup group : VikimgApp.buddyList) {
			for (BuddyEntry entry : group.getBuddyList()) {
				entry.setStatus(BuddyStatus.OFFLINE);
			}
			group.setOnlineRatio("(0/" + group.getBuddyList().size() + ")");
		}
	}

	public static String disconnect() {
		try {
			if (VikimgApp.roster != null) {
				VikimgApp.roster.removeRosterListener(VikimgApp.rosterListener);
			}
			VikimgApp.connection.disconnect();
			VikimgApp.buddyList.clear();
			VikimgApp.chatList.clear();
		} catch (Exception e) {
			e.printStackTrace();
			return "20";
		}
		VikimgApp.isUserAvailable = false;
		return "10";

	}

	public static String logout() {
		if (VikimgApp.connection.isConnected()) {
			Presence presence = new Presence(Type.unavailable);
			VikimgApp.isUserAvailable = false;
			SharedPreferences settings = VikimgApp.currentActivity.getSharedPreferences(VikimgApp.user + "_" + VikimgApp.STATUS_PREF, 0);
			String description = settings.getString("description", "default");
			if (!description.equals("default")) {
				presence.setStatus(description);
			}
//			VikimgApp.buddyList.clear();
			VikimgApp.chatList.clear();
			VikimgApp.connection.sendPacket(presence);
			VikimgApp.connection.disconnect();
			return "10";
		} else {
			VikimgApp.isUserAvailable = false;
			setOfflineBuddyList();
			return "20";
		}

	}

	public static String differentConnect() {
		SharedPreferences settings = VikimgApp.currentActivity.getSharedPreferences(VikimgApp.user + "_"
				+ VikimgApp.STATUS_PREF, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("status", 1);
		editor.commit();
		VikimgApp.isUserAvailable = true;
		Presence presence = new Presence(Type.available);
		VikimgApp.connection.sendPacket(presence);
		return "0";
	}
}
