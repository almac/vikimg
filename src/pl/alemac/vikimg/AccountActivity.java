package pl.alemac.vikimg;

import pl.alemac.vikimg.account.AccountFragment;
import pl.alemac.vikimg.account.EditMainAccountFragment;
import pl.alemac.vikimg.account.EditXmppAccountFragment;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class AccountActivity extends SherlockFragmentActivity {
	private ActionBar mActionBar;
	private SherlockFragment accountFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account);
		VikimgApp.currentActivity = this;
		VikimgViewUtils.showRunningNotification(this);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle("Konto");
		accountFragment = AccountFragment.newInstance();
		SherlockFragment existingFragment = (SherlockFragment) getSupportFragmentManager().findFragmentById(
				R.id.account_fragment_container);
		if (existingFragment == null || existingFragment.getClass().equals(AccountFragment.class)) {

			getSupportFragmentManager().beginTransaction()
					.replace(R.id.account_fragment_container, accountFragment, "accountFragment").commit();

		} else if (existingFragment.getClass().equals(EditMainAccountFragment.class)) {
			SherlockFragment editMainAccountFragment = EditMainAccountFragment.newInstance();
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.account_fragment_container, editMainAccountFragment, "editMainAccountFragment")
					.commit();

		} else if (existingFragment.getClass().equals(EditXmppAccountFragment.class)) {
			SherlockFragment editXmppAccountFragment = EditXmppAccountFragment.newInstance();
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.account_fragment_container, editXmppAccountFragment, "editXmppAccountFragment")
					.commit();
		}

	}

	public boolean onOptionsItemSelected(MenuItem item) {

		if (getSupportFragmentManager().findFragmentByTag("editMainAccountFragment") != null) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.account_fragment_container, accountFragment, "accountFragment").commit();
		} else if (getSupportFragmentManager().findFragmentByTag("editXmppAccountFragment") != null) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.account_fragment_container, accountFragment, "accountFragment").commit();
		} else {
			startActivity(new Intent(AccountActivity.this, MainScreen.class));
			finish();
		}

		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (getSupportFragmentManager().findFragmentByTag("editMainAccountFragment") != null) {
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.account_fragment_container, accountFragment, "accountFragment").commit();
			} else if (getSupportFragmentManager().findFragmentByTag("editXmppAccountFragment") != null) {
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.account_fragment_container, accountFragment, "accountFragment").commit();
			} else {
				startActivity(new Intent(AccountActivity.this, MainScreen.class));
				finish();
			}
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

}
