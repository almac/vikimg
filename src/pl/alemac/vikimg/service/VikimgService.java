package pl.alemac.vikimg.service;

import android.app.Service;
import android.content.Intent;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class VikimgService extends Service {

	public static final String ACTION = "pl.alemac.vikimg.action.action";
	public static final String REACTION = "pl.alemac.vikimg.service.VikimgService.reaction";
	public static final String REREACTION = "pl.alemac.vikimg.service.VikimgService.rereaction";

	public static final String START = "pl.alemac.vikimg.action.start";
	public static final String STOP = "pl.alemac.vikimg.action.stop";

	private String user;
//	private Looper sServiceLooper;
//	private ServiceHandler sServiceHandler;
//	private long mHandlerThreadId;
//
//	private class ServiceHandler extends Handler {
//		public ServiceHandler(Looper looper) {
//			super(looper);
//		}
//
//		@Override
//		public void handleMessage(android.os.Message msg) {
//			onHandleIntent((Intent) msg.obj, msg.arg1);
//			Log.d("test", "SERVICE HANDLER called ACTION");
//		}
//	}
//
//	void onHandleIntent(final Intent intent, int id) {
//		if (Thread.currentThread().getId() != mHandlerThreadId) {
//			throw new IllegalThreadStateException();
//		}
//		Log.d("test", "onHandleIntent called ACTION");
//		String test = intent.getAction();
//		if (test.equals(ACTION)) {
//			Log.d("test", "onHandleIntent called ACTION");
//			Intent in = new Intent();
//			in.setAction(REACTION);
//			in.putExtra("user", user);
//			Log.d("test", "sending broadcast REACTION");
//			LocalBroadcastManager.getInstance(this).sendBroadcast(in);
//		} else if (test.equals(REREACTION)) {
//			Log.d("test", "onHandleIntent called REREACTION");
//			user = intent.getStringExtra("user");
//			Log.d("test", "final user " + user);
//		} else if (test.equals(START)) {
//			Log.d("service", "Service started");
//		} else if (test.equals(STOP)) {
//			Log.d("service", "Service stopped");
//			stopSelf();
//		}
//	}

	@Override
	public void onCreate() {
		super.onCreate();
		// user = "test333";
		// Log.d("test", "service on create");
		// Start a new thread for the service
		HandlerThread thread = new HandlerThread("VikIMg.Service");
		thread.start();
//		mHandlerThreadId = thread.getId();
//		sServiceLooper = thread.getLooper();
//		sServiceHandler = new ServiceHandler(sServiceLooper);

		// BroadcastReceiver onNotice = new BroadcastReceiver() {
		//
		// @Override
		// public void onReceive(Context context, Intent intent) {
		//
		// // Log.d("sohail", intent.getAction());
		// if (intent.getAction().equals(VikimgService.REACTION)) {
		// Log.d("sohail", "onReceive called REACTION");
		// Intent in = new Intent(getApplicationContext(), VikimgService.class);
		// in.setAction(VikimgService.REREACTION);
		// in.putExtra("user", "test66246");
		// Log.d("sohail", "sending broadcast REREACTION");
		// startService(in);
		// // LocalBroadcastManager.getInstance(inn).sendBroadcast(in);
		// }
		// }
		// };

	}

	public int onStartCommand(Intent intent, int flags, int startId) {

		String test = intent.getAction();
		if (test.equals(ACTION)) {
			Log.d("test", "onHandleIntent called ACTION");
			Intent in = new Intent();
			in.setAction(REACTION);
			in.putExtra("user", user);
			Log.d("test", "sending broadcast REACTION");
			LocalBroadcastManager.getInstance(this).sendBroadcast(in);
		} else if (test.equals(REREACTION)) {
			Log.d("sohail", "onHandleIntent called REREACTION");
			user = intent.getStringExtra("user");
			Log.d("test", "final user " + user);
		} else if (test.equals(START)) {
			Log.d("service", "Service started");
		} else if (test.equals(STOP)) {
			Log.d("service", "Service stopped");
			stopSelf();
		}
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("test", "service destroyed");
	}
}

// -----------------------------------------------
//
// public class VikimgService extends IntentService {
//
// private String user;
//
// @Override
// public int onStartCommand(Intent intent, int flags, int startId) {
// super.onStartCommand(intent, flags, startId);
// return START_STICKY;
// }
//
// public static final String ACTION = "act";
// public static final String REACTION = "react";
// public static final String REREACTION = "rereact";

// public VikimgService() {
// super("VikimgService");
// Log.d("sohail", "service started");
// user = "test333";
// }
//
// @Override
// protected void onHandleIntent(Intent arg0) {
// String test = arg0.getAction();
// if (test.equals(ACTION)) {
// Log.d("sohail", "onHandleIntent called ACTION");
// Intent in = new Intent();
// in.setAction(REACTION);
// in.putExtra("user", user);
// Log.d("sohail", "sending broadcast REACTION");
// LocalBroadcastManager.getInstance(this).sendBroadcast(in);
// } else if (test.equals(REREACTION)) {
// Log.d("sohail", "onHandleIntent called REREACTION");
// user = arg0.getStringExtra("user");
// Log.d("test", "final user " + user);
// }
// }
//
// @Override
// public IBinder onBind(Intent arg0) {
// // TODO Auto-generated method stub
// return null;
// }
//
// @Override
// public void onDestroy() {
// super.onDestroy();
// Log.e("test", "service destroyed");
// }
// }