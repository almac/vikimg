package pl.alemac.vikimg;

import pl.alemac.vikimg.buddy.BuddyStatus;
import pl.alemac.vikimg.buddyList.NewBuddyActivity;
import pl.alemac.vikimg.service.VikimgService;
import pl.alemac.vikimg.settings.SettingsActivity;
import pl.alemac.vikimg.utilities.VikimgLoginUtils;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import pl.alemac.vikimg.utilities.vikIMgFragmentPagerAdapter;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class MainScreen extends SherlockFragmentActivity {
	ActionBar mActionBar;
	ViewPager mPager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);
		VikimgApp.currentActivity = this;
		VikimgApp.buddyListActivity = this;
		VikimgViewUtils.showRunningNotification(this);
		mActionBar = getSupportActionBar();
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		mPager = (ViewPager) findViewById(R.id.pager);
		FragmentManager fm = getSupportFragmentManager();
		ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				mActionBar.setSelectedNavigationItem(position);
			}
		};

		
//		onNotice = new BroadcastReceiver() {
//			@Override
//			public void onReceive(Context context, Intent intent) {	
//				if (intent.getAction().equals(VikimgService.REACTION)) {
//					Log.d("test", "onReceive called REACTION MAIN");
//					Log.d("test", intent.getStringExtra("user"));
//					Toast.makeText(getApplicationContext(), intent.getStringExtra("user"), Toast.LENGTH_LONG).show();
//				}
//			}
//		};
//		IntentFilter iff2 = new IntentFilter(VikimgService.REACTION);
//		LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, iff2);
//		
//		Intent i = new Intent(this, VikimgService.class);
//		i.setAction(VikimgService.ACTION);
//		startService(i);
		
		mPager.setOnPageChangeListener(pageChangeListener);
		vikIMgFragmentPagerAdapter fragmentPagerAdapter = new vikIMgFragmentPagerAdapter(fm);
		mPager.setAdapter(fragmentPagerAdapter);
		mActionBar.setDisplayShowTitleEnabled(true);

		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			}

			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				mPager.setCurrentItem(tab.getPosition());
			}

			public void onTabReselected(Tab tab, FragmentTransaction ft) {
			}
		};

		Tab tab = mActionBar.newTab().setText("Kontakty").setTabListener(tabListener);
		mActionBar.addTab(tab);
		tab = mActionBar.newTab().setText("Rozmowy").setTabListener(tabListener);
		mActionBar.addTab(tab);
		mPager.setCurrentItem(VikimgApp.currentTab);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater minflater = getSupportMenuInflater();
		minflater.inflate(R.menu.activity_main_screen, menu);

		if (!VikimgApp.isUserAvailable) {
			MenuItem item = menu.getItem(0);
			item.setIcon(BuddyStatus.values()[5].getResource());
		} else {
			SharedPreferences settings = getSharedPreferences(VikimgApp.user + "_" + VikimgApp.STATUS_PREF, 0);
			int status = settings.getInt("status", -1);
			if (status != -1) {
				MenuItem item = menu.getItem(0);
				item.setIcon(BuddyStatus.values()[status].getResource());
			}
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.presence:
				Intent intent = new Intent(MainScreen.this, EditStatusActivity.class);
				MainScreen.this.startActivity(intent);
				finish();
				return true;

			case R.id.new_buddy:
				if (VikimgApp.isUserAvailable) {
					Intent intent2 = new Intent(MainScreen.this, NewBuddyActivity.class);
					MainScreen.this.startActivity(intent2);
					finish();
				} else {
					VikimgViewUtils.showErrorDialog(this, getResources().getString(R.string.no_connection));
				}

				return true;

			case R.id.settings:
				Intent intent3 = new Intent(MainScreen.this, SettingsActivity.class);
				MainScreen.this.startActivity(intent3);
				finish();
				return true;

			case R.id.account:
				Intent intent4 = new Intent(MainScreen.this, AccountActivity.class);
				MainScreen.this.startActivity(intent4);
				finish();
				return true;

			case R.id.archive:
				Intent intent5 = new Intent(MainScreen.this, ArchiveActivity.class);
				MainScreen.this.startActivity(intent5);
				finish();
				return true;

			case R.id.logout:
				createLogoutDialog();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void createLogoutDialog() {
		final AlertDialog.Builder b = new AlertDialog.Builder(this);

		b.setMessage("Czy zakonczyc dzialanie komunikatora?");
		b.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Intent i = new Intent(getApplicationContext(), VikimgService.class);
				i.setAction(VikimgService.STOP);
				startService(i);

				LogoutTask task = new LogoutTask();
				task.execute();

				finish();
			}
		});
		b.setNegativeButton("Nie", null);

		b.show();

	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		// your stuff or nothing
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		// your stuff or nothing
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			moveTaskToBack(true);
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		VikimgApp.currentTab = mActionBar.getSelectedNavigationIndex();
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

	private class LogoutTask extends AsyncTask<String, String, String> {
		private ProgressDialog dialog;

		public LogoutTask() {
			dialog = new ProgressDialog(MainScreen.this);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getResources().getString(R.string.progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			VikimgLoginUtils.disconnect();

			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.cancelAll();

			return "0";
		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);
			dialog.dismiss();

		}

	}

}
