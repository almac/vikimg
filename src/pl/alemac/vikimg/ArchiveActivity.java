package pl.alemac.vikimg;

import pl.alemac.vikimg.archive.BuddyArchiveFragment;
import pl.alemac.vikimg.archive.ChatArchiveFragment;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class ArchiveActivity extends SherlockFragmentActivity {
	private ActionBar mActionBar;
	private SherlockFragment buddyArchiveFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_archive);
		VikimgApp.currentActivity = this;
		VikimgViewUtils.showRunningNotification(this);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle("Archiwum wiadomosci");

		buddyArchiveFragment = BuddyArchiveFragment.newInstance();
		SherlockFragment existingFragment = (SherlockFragment) getSupportFragmentManager().findFragmentById(
				R.id.frag_containter);
		if (existingFragment == null || existingFragment.getClass().equals(BuddyArchiveFragment.class)) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.frag_containter, buddyArchiveFragment, "buddyArchiveFragment").commit();
			
		} else if (existingFragment.getClass().equals(ChatArchiveFragment.class)) {
			SherlockFragment chatArchiveFragment = ChatArchiveFragment.newInstance();
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.frag_containter, chatArchiveFragment, "chatArchiveFragment").commit();
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		if (getSupportFragmentManager().findFragmentByTag("chatArchiveFragment") != null) {
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.frag_containter, buddyArchiveFragment, "buddyArchiveFragment").commit();
			return true;
		} else {
			startActivity(new Intent(ArchiveActivity.this, MainScreen.class));
			finish();
			return true;
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (getSupportFragmentManager().findFragmentByTag("chatFragment") != null) {
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.frag_containter, buddyArchiveFragment, "buddyArchiveFragment").commit();
				return true;
			} else {
				startActivity(new Intent(ArchiveActivity.this, MainScreen.class));
				finish();
				return true;
			}
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

}
