package pl.alemac.vikimg.chatList;

import java.util.ArrayList;

import org.jivesoftware.smack.Chat;

import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.message.MessageItem;

public class ChatItem {

	private String chatOwner;
	private String participantJid;
	private String participantName;
	private Chat chat;
	private ArrayList<MessageItem> messageList;
	private boolean newMessage;

	public ChatItem() {
	}

	public ChatItem(String owner, String buddyJid, String buddyName, boolean message) {
		chatOwner = owner;
		participantJid = buddyJid;
		participantName = setName(buddyJid, buddyName);
		setMessageList(new ArrayList<MessageItem>());
		newMessage = true;
		chat = VikimgApp.connection.getChatManager().createChat(buddyJid, VikimgApp.messageListener);
	}

	public ChatItem(String owner, String buddyJid, String buddyName) {
		chatOwner = owner;
		participantJid = buddyJid;
		participantName = setName(buddyJid, buddyName);
		setMessageList(new ArrayList<MessageItem>());
		newMessage = false;
		chat = VikimgApp.connection.getChatManager().createChat(buddyJid, VikimgApp.messageListener);
	}

	public ChatItem(String owner, String buddyJid, String buddyName, Chat chat) {
		chatOwner = owner;
		participantJid = buddyJid;
		participantName = setName(buddyJid, buddyName);
		setMessageList(new ArrayList<MessageItem>());
		newMessage = false;
		this.chat = chat;
	}

	public ChatItem(String owner, String buddyJid, String buddyName, Chat chat, boolean message) {
		chatOwner = owner;
		participantJid = buddyJid;
		participantName = setName(buddyJid, buddyName);
		newMessage = message;
		setMessageList(new ArrayList<MessageItem>());
		this.chat = chat;
	}

	public String getChatOwner() {
		return chatOwner;
	}

	public void setChatOwner(String chatOwner) {
		this.chatOwner = chatOwner;
	}

	public boolean isNewMessage() {
		return newMessage;
	}

	public void setNewMessage(boolean newMessage) {
		this.newMessage = newMessage;
	}

	public Chat getChat() {
		return chat;
	}

	public void setChat(Chat chat) {
		this.chat = chat;
	}

	public ArrayList<MessageItem> getMessageList() {
		return messageList;
	}

	public void setMessageList(ArrayList<MessageItem> messageList) {
		this.messageList = messageList;
	}

	public String getParticipantJid() {
		return participantJid;
	}

	public void setParticipantJid(String participantJid) {
		this.participantJid = participantJid;
	}

	public String getParticipantName() {
		return participantName;
	}

	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}

	private String setName(String jid, String name) {
		if (name != null) {
			return name;
		} else {
			return jid;
		}

	}

}
