package pl.alemac.vikimg.chatList;

import pl.alemac.vikimg.ChatActivity;
import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;

public class ChatListFragment extends SherlockFragment {

	private ListView chatListView;
	private final static int CHAT_TALK = 1;
	private final static int CHAT_REMOVE = 2;
	private final static int CHAT_CONTEXT_MENU = 1;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_chat_list, container, false);
		return view;
	}

	public void onStart() {
		super.onStart();
		VikimgApp.chatListAdapter = new ChatListAdapter(getActivity(), R.layout.chat_list_row, VikimgApp.chatList);
		chatListView = (ListView) getActivity().findViewById(R.id.chat_list);
		chatListView.setAdapter(VikimgApp.chatListAdapter);
		registerForContextMenu(chatListView);
		setListeners();
	}

	private void setListeners() {
		chatListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent intent = new Intent(getActivity(), ChatActivity.class);
				intent.putExtra("position", Integer.toString(arg2));
				startActivity(intent);
				getActivity().finish();
			}
		});

	}

	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		if (v.getId() == R.id.chat_list) {
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle(VikimgApp.chatList.get(info.position).getParticipantName());
			menu.add(CHAT_CONTEXT_MENU, CHAT_TALK, 0, "Rozmowa");
			menu.add(CHAT_CONTEXT_MENU, CHAT_REMOVE, 0, "Usun");
		}
	}

	public boolean onContextItemSelected(MenuItem menuItem) {

		if (menuItem.getGroupId() == CHAT_CONTEXT_MENU) {

			AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) menuItem.getMenuInfo();

			switch (menuItem.getItemId()) {
				case CHAT_TALK:
					Intent intent = new Intent(getActivity(), ChatActivity.class);
					intent.putExtra("position", Integer.toString(menuInfo.position));
					startActivity(intent);
					getActivity().finish();
					return true;

				case CHAT_REMOVE:
					VikimgApp.chatList.remove(menuInfo.position);
					VikimgApp.chatListAdapter.notifyDataSetInvalidated();
					return true;

				default:
					return super.onContextItemSelected(menuItem);

			}
		}
		return false;
	}

}
