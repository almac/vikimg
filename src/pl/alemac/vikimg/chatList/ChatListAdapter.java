package pl.alemac.vikimg.chatList;

import java.util.ArrayList;

import pl.alemac.vikimg.R;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChatListAdapter extends ArrayAdapter<ChatItem> {

	private int resource;
	private LayoutInflater inflater;
	private ArrayList<ChatItem> chatList;

	public ChatListAdapter(Context _context, int textViewResourceId, ArrayList<ChatItem> objects) {
		super(_context, textViewResourceId, objects);
		resource = textViewResourceId;
		inflater = LayoutInflater.from(_context);
		chatList = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = inflater.inflate(resource, parent, false);
		TextView participant = (TextView) rowView.findViewById(R.id.participant);
		ImageView newMessage = (ImageView) rowView.findViewById(R.id.new_message_info);

		String name = chatList.get(position).getParticipantName();

		if (TextUtils.isEmpty(name)) {
			name = chatList.get(position).getParticipantJid();
		}

		participant.setText(name);

		if (chatList.get(position).isNewMessage()) {
			newMessage.setImageResource(R.drawable.new_message);

		} else {
			newMessage.setImageResource(R.drawable.blank);
		}
		return rowView;
	}

}
