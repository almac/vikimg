package pl.alemac.vikimg.buddy;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;

import pl.alemac.vikimg.R;

public enum BuddyStatus {
	LETS_TALK("Porozmawiajmy", R.drawable.status_letstalk), AVAILABLE("Dostepny", R.drawable.status_online), BRB(
			"Zaraz wracam", R.drawable.status_brb), BRB_LATER("Wroce pozniej", R.drawable.status_brb_later), BUSY(
			"Zajety", R.drawable.status_busy), OFFLINE("Niedostepny", R.drawable.status_offline);

	private String name;
	private Integer resource;

	BuddyStatus(String name, Integer resource) {
		this.name = name;
		this.resource = resource;
	}

	public String getName() {
		return name;
	}

	public Integer getResource() {
		return resource;
	}

	public static BuddyStatus retrieveStatus(Presence presence) {
		if (presence.isAvailable() == true) {
			if (presence.getMode() == Mode.available || presence.getMode() == null) {
				return BuddyStatus.AVAILABLE;
			}

			if (presence.getMode() == Mode.chat) {
				return BuddyStatus.LETS_TALK;
			}

			if (presence.getMode() == Mode.dnd) {
				return BuddyStatus.BUSY;
			}

			if (presence.getMode() == Mode.away) {
				return BuddyStatus.BRB;
			}

			if (presence.getMode() == Mode.xa) {
				return BuddyStatus.BRB_LATER;
			}
		} else {
			return BuddyStatus.OFFLINE;
		}

		return null;
	}

	public static Presence.Mode retrievePresence(BuddyStatus status) {

		if (status.equals(LETS_TALK)) {
			return Presence.Mode.chat;
		}
		if (status.equals(AVAILABLE)) {
			return Presence.Mode.available;
		}
		if (status.equals(BRB)) {
			return Presence.Mode.away;
		}
		if (status.equals(BRB_LATER)) {
			return Presence.Mode.xa;
		}
		if (status.equals(BUSY)) {
			return Presence.Mode.dnd;
		}
//		if (status.equals(OFFLINE)) {
//			return Presence.Type.unavailable;
//		}
		return null;
	}

}
