package pl.alemac.vikimg.buddy;

import java.util.Comparator;

public class BuddyStatusComparator implements Comparator<BuddyEntry> {

	@Override
	public int compare(BuddyEntry arg0, BuddyEntry arg1) {
		return arg0.getStatus().ordinal()- arg1.getStatus().ordinal();
	}

}
