package pl.alemac.vikimg.buddy;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;


public class BuddyEntryComparator implements Comparator<BuddyEntry> {

	public int compare(BuddyEntry arg0, BuddyEntry arg1) {

		Collator c = Collator.getInstance(Locale.getDefault());
		return c.compare(arg0.getName(), arg1.getName());
	}

}
