package pl.alemac.vikimg.buddy;

import java.util.ArrayList;

public class BuddyGroup {

	private String name;
	private ArrayList<BuddyEntry> buddyList;
	private String onlineRatio;

	public BuddyGroup(String name) {
		super();
		this.name = name;
		this.buddyList = new ArrayList<BuddyEntry>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<BuddyEntry> getBuddyList() {
		return buddyList;
	}

	public void setBuddyList(ArrayList<BuddyEntry> buddyList) {
		this.buddyList = buddyList;
	}

	public String getOnlineRatio() {
		return onlineRatio;
	}

	public void setOnlineRatio(String string) {
		onlineRatio = string;
	}
}
