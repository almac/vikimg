package pl.alemac.vikimg.buddy;

import java.io.Serializable;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;

@SuppressWarnings("serial")
public class BuddyEntry implements Serializable {

	private String name;
	private String jid;
	private String description;
	private BuddyStatus status;

	public BuddyEntry(String name, String jid, String description, BuddyStatus status) {
		if (name == null) {
			this.name = jid;
		} else {
			this.name = name;
		}
		this.jid = jid;
		this.description = description;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BuddyStatus getStatus() {
		return status;
	}

	public void setStatus(BuddyStatus status) {
		this.status = status;
	}

	public void setStatusFromRosterEntry(Presence presence) {
		if (presence.isAvailable() == true) {
			if (presence.getMode() == Mode.available || presence.getMode() == null) {
				status = BuddyStatus.AVAILABLE;
			}

			if (presence.getMode() == Mode.chat) {
				status = BuddyStatus.LETS_TALK;
			}

			if (presence.getMode() == Mode.dnd) {
				status = BuddyStatus.BUSY;
			}

			if (presence.getMode() == Mode.away) {
				status = BuddyStatus.BRB;
			}

			if (presence.getMode() == Mode.xa) {
				status = BuddyStatus.BRB_LATER;
			}
		} else {
			status = BuddyStatus.OFFLINE;
		}
	}

}
