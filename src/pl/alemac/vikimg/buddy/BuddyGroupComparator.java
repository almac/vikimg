package pl.alemac.vikimg.buddy;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class BuddyGroupComparator implements Comparator<BuddyGroup> {

	public int compare(BuddyGroup arg0, BuddyGroup arg1) {

		Collator c = Collator.getInstance(Locale.getDefault());
		return c.compare(arg0.getName(), arg1.getName());
	}

}
