package pl.alemac.vikimg.database.helper;

import java.util.ArrayList;
import java.util.Date;

import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.archive.model.BuddyArchiveBinder;
import pl.alemac.vikimg.database.model.Account;
import pl.alemac.vikimg.database.model.XmppAccount;
import pl.alemac.vikimg.message.MessageItem;
import pl.alemac.vikimg.utilities.VikimgTimeUtils;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class VikimgDatabaseHelper extends SQLiteOpenHelper {

	private static String LOG = "database";

	private static String DATABASE_NAME = "vikimgDatabase";
	private static final int DATABASE_VERSION = 1;

	private static String TABLE_ACCOUNT = "account";
	private static String TABLE_XMPP_ACCOUNT = "xmpp_account";
	private static String TABLE_ARCHIVE_MESSAGES = "archive_messages";
	private static String TABLE_ARCHIVE_BUDDY = "archive_buddy";

	private static String KEY_ID = "id";
	private static String KEY_NICK = "nick";
	private static String KEY_PASSWORD = "password";
	private static String KEY_JID = "jid";
	private static String KEY_MESSAGE = "message";
	private static String KEY_DATE = "date";
	private static String KEY_SENDER = "sender";

	private static String CREATE_TABLE_ACCOUNT = "CREATE TABLE " + TABLE_ACCOUNT + " (" + KEY_ID
			+ " INTEGER PRIMARY KEY, " + KEY_NICK + " TEXT," + KEY_PASSWORD + " TEXT)";
	private static String CREATE_TABLE_XMPP_ACCOUNT = "CREATE TABLE " + TABLE_XMPP_ACCOUNT + " (" + KEY_ID
			+ " INTEGER, " + KEY_JID + " TEXT," + KEY_PASSWORD + " TEXT,"
			+ "FOREIGN KEY("+KEY_ID+") REFERENCES "+TABLE_ACCOUNT+"("+KEY_ID+")" + ")";

	public VikimgDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_ACCOUNT);
		db.execSQL(CREATE_TABLE_XMPP_ACCOUNT);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_XMPP_ACCOUNT);

		onCreate(db);
	}

	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen())
			db.close();
	}

	public long createAccount(Account account, XmppAccount xmppAccount) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NICK, account.getNick());
		values.put(KEY_PASSWORD, account.getPassword());

		long account_id = db.insert(TABLE_ACCOUNT, null, values);

		xmppAccount.setId((int) account_id);

		createXmppAccount(xmppAccount);

		return account_id;
	}

	public Account getAccountByNick(String nick) {
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT * FROM " + TABLE_ACCOUNT + " WHERE " + KEY_NICK + " = ?";

		// Log.e(LOG, selectQuery);

		Cursor c = db.rawQuery(selectQuery, new String[] { nick });
		Account account = new Account();
		if (c.moveToFirst()) {
			c.moveToFirst();
			account.setId(c.getInt(c.getColumnIndex(KEY_ID)));
			account.setNick((c.getString(c.getColumnIndex(KEY_NICK))));
			account.setPassword(c.getString(c.getColumnIndex(KEY_PASSWORD)));
		} else {
			account = null;
		}

		return account;
	}

	public long createXmppAccount(XmppAccount xmppAccount) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ID, xmppAccount.getId());
		values.put(KEY_JID, xmppAccount.getJid());
		values.put(KEY_PASSWORD, xmppAccount.getPassword());

		long account_id = db.insert(TABLE_XMPP_ACCOUNT, null, values);

		return account_id;
	}

	public XmppAccount getXmppAccountByNick(String nick) {
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT " + TABLE_XMPP_ACCOUNT + "." + KEY_ID + " , " + KEY_JID + " , "
				+ TABLE_XMPP_ACCOUNT + "." + KEY_PASSWORD + " FROM " + TABLE_XMPP_ACCOUNT + " JOIN " + TABLE_ACCOUNT
				+ " ON " + TABLE_XMPP_ACCOUNT + "." + KEY_ID + "=" + TABLE_ACCOUNT + "." + KEY_ID + " WHERE "
				+ TABLE_ACCOUNT + "." + KEY_NICK + " = ?";

		Log.e(LOG, selectQuery);

		XmppAccount account = new XmppAccount();
		Cursor c = db.rawQuery(selectQuery, new String[] { nick });
		if (c.moveToFirst()) {
			c.moveToFirst();
			account.setId(c.getInt(c.getColumnIndex(KEY_ID)));
			account.setJid((c.getString(c.getColumnIndex(KEY_JID))));
			account.setPassword(c.getString(c.getColumnIndex(KEY_PASSWORD)));
		} else {
			account = null;
		}

		return account;
	}

	public long updateXmppAccount(String oldJid, String jid, String md5hash) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Log.e(LOG, "update " + jid);

		ContentValues values = new ContentValues();
		values.put(KEY_JID, jid);
		values.put(KEY_PASSWORD, md5hash);

		long buddyId = db.update(TABLE_XMPP_ACCOUNT, values, KEY_JID + " = '" + oldJid + "'", null);
		if (db != null && db.isOpen())
			db.close();
		return buddyId;
	}

	public long updateAccountPassword(String md5hash) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Log.e(LOG, "update " + jid);

		ContentValues values = new ContentValues();
		values.put(KEY_PASSWORD, md5hash);

		long buddyId = db.update(TABLE_ACCOUNT, values, KEY_NICK + " = '" + VikimgApp.user + "'", null);
		if (db != null && db.isOpen())
			db.close();
		return buddyId;
	}

	public long createArchiveBuddy(String jid, String nick) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Log.e(LOG, nick + " " + jid);

		ContentValues values = new ContentValues();

		values.put(KEY_JID, jid);
		values.put(KEY_NICK, nick);

		long buddyId = db.insertWithOnConflict(VikimgApp.user + "_" + TABLE_ARCHIVE_BUDDY, null, values,
				SQLiteDatabase.CONFLICT_IGNORE);
		if (db != null && db.isOpen())
			db.close();
		return buddyId;
	}

	public long updateArchiveBuddy(String nick, String jid) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Log.e(LOG, "update " + jid);

		ContentValues values = new ContentValues();
		values.put(KEY_NICK, nick);

		long buddyId = db
				.update(VikimgApp.user + "_" + TABLE_ARCHIVE_BUDDY, values, KEY_JID + " = '" + jid + "'", null);
		if (db != null && db.isOpen())
			db.close();
		return buddyId;
	}

	// CRUD XMPP ACCOUNT
	public long createMessage(String id, String sender, String message, Date date) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Log.e(LOG, sender + " " + message + " " + date.toString());

		ContentValues values = new ContentValues();
		values.put(KEY_ID, id);
		values.put(KEY_SENDER, sender);
		values.put(KEY_MESSAGE, message);
		values.put(KEY_DATE, VikimgTimeUtils.getFullDate(date));

		long message_id = db.insert(VikimgApp.user + "_" + TABLE_ARCHIVE_MESSAGES, null, values);
		if (db != null && db.isOpen())
			db.close();
		return message_id;
	}

	public ArrayList<BuddyArchiveBinder> getDistinctArchiveBuddy() {
		ArrayList<BuddyArchiveBinder> buddyNick = new ArrayList<BuddyArchiveBinder>();
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT " + KEY_JID + " , " + KEY_NICK + " FROM " + VikimgApp.user + "_"
				+ TABLE_ARCHIVE_BUDDY;
		// Log.e("database", selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		while (c.moveToNext()) {
			String nick = c.getString(c.getColumnIndex(KEY_NICK));
			String jid = c.getString(c.getColumnIndex(KEY_JID));
			// Log.e(LOG, nick + " " + jid);
			buddyNick.add(new BuddyArchiveBinder(nick, jid));
		}
		return buddyNick;
	}

	public ArrayList<String> getDistinctBuddyDates(String jid) {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<String> dateList = new ArrayList<String>();
		String selectQuery = "SELECT DISTINCT " + KEY_ID + " FROM " + VikimgApp.user + "_" + TABLE_ARCHIVE_MESSAGES
				+ " WHERE " + KEY_ID + " LIKE '%" + jid + "%'";
		// Log.e(LOG, selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		while (c.moveToNext()) {
			String data[] = c.getString(c.getColumnIndex(KEY_ID)).split("_");
			// Log.e(LOG, data[data.length - 1]);
			dateList.add(data[data.length - 1]);
		}
		return dateList;
	}

	public String getArchiveBuddyNick(String jid) {
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT " + KEY_NICK + " FROM " + VikimgApp.user + "_" + TABLE_ARCHIVE_BUDDY + " WHERE "
				+ KEY_JID + " = " + "'" + jid + "'";
		// Log.e(LOG, selectQuery);
		String nick = new String();
		Cursor c = db.rawQuery(selectQuery, null);
		while (c.moveToNext()) {
			nick = c.getString(c.getColumnIndex(KEY_NICK));
		}
		// Log.e(LOG, "selected nick " + nick + " for jid " + jid);
		return nick;

	}

	public void deleteAllMessages() {
		SQLiteDatabase db = this.getWritableDatabase();

		String deleteArchiveBuddy = "DELETE FROM " + VikimgApp.user + "_" + TABLE_ARCHIVE_BUDDY;
		String deleteArchiveMessages = "DELETE FROM " + VikimgApp.user + "_" + TABLE_ARCHIVE_MESSAGES;

		db.execSQL(deleteArchiveBuddy);
		db.execSQL(deleteArchiveMessages);

		if (db != null && db.isOpen())
			db.close();

	}

	public ArrayList<MessageItem> getSpecficMessages(String jid, String date) {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<MessageItem> messageList = new ArrayList<MessageItem>();

		String selectNick = "SELECT " + KEY_NICK + " FROM " + VikimgApp.user + "_" + TABLE_ARCHIVE_BUDDY + " WHERE "
				+ KEY_JID + " = " + "'" + jid + "'";
		// Log.e(LOG, selectNick);
		String sender = new String();
		String nick = new String();
		Cursor c1 = db.rawQuery(selectNick, null);
		while (c1.moveToNext()) {
			nick = c1.getString(c1.getColumnIndex(KEY_NICK));
		}
		// Log.e(LOG, "selected nick " + nick + " for jid " + jid);

		String selectQuery = "SELECT " + KEY_SENDER + " , " + KEY_MESSAGE + " , " + KEY_DATE + " FROM "
				+ VikimgApp.user + "_" + TABLE_ARCHIVE_MESSAGES + " WHERE " + KEY_ID + " = " + "'" + jid + "_" + date
				+ "'";
		Log.e(LOG, selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		while (c.moveToNext()) {
			MessageItem item = new MessageItem();
			sender = c.getString(c.getColumnIndex(KEY_SENDER));
			if (sender.equals(VikimgApp.user)) {
				item.setSender(sender);
			} else {
				item.setSender(nick);
			}

			item.setMessage(c.getString(c.getColumnIndex(KEY_MESSAGE)));
			item.setDate(VikimgTimeUtils.getFullDate(c.getString(c.getColumnIndex(KEY_DATE))));
			messageList.add(item);
			// Log.e(LOG, item.getSender() + " " + item.getMessage() + " " +
			// item.getDate());
		}

		return messageList;
	}

	public void createAccountArchive(String nick) {
		SQLiteDatabase db = this.getWritableDatabase();
		String createAccountArchiveMessage = "CREATE TABLE " + nick + "_" + TABLE_ARCHIVE_MESSAGES + " (" + KEY_ID
				+ " TEXT, " + KEY_SENDER + " TEXT, " + KEY_MESSAGE + " TEXT, " + KEY_DATE + " TEXT)";
		db.execSQL(createAccountArchiveMessage);

		String createAccountArchiveBuddy = "CREATE TABLE " + nick + "_" + TABLE_ARCHIVE_BUDDY + " (" + KEY_JID
				+ " TEXT UNIQUE, " + KEY_NICK + " TEXT)";
		db.execSQL(createAccountArchiveBuddy);

	}

	public static String setMessageId(String jid) {
		return jid + "_" + VikimgTimeUtils.getDate(new Date());
	}

}
