package pl.alemac.vikimg.database.model;

public class XmppAccount {

	private int id;
	private String jid;
	private String password;

	public XmppAccount() {
	}

	public XmppAccount(int id, String jid, String password) {
		this.id = id;
		this.jid = jid;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
