package pl.alemac.vikimg.xmpp.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Presence;

import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.buddy.BuddyEntry;
import pl.alemac.vikimg.buddy.BuddyEntryComparator;
import pl.alemac.vikimg.buddy.BuddyGroup;
import pl.alemac.vikimg.buddy.BuddyGroupComparator;
import pl.alemac.vikimg.buddy.BuddyStatus;
import pl.alemac.vikimg.buddy.BuddyStatusComparator;
import android.util.Log;

public class BuddyListener implements RosterListener {

	@Override
	public void entriesAdded(Collection<String> entries) {
	}

	@Override
	public void entriesDeleted(Collection<String> entries) {
		Log.d("test", "delete");
		ArrayList<BuddyGroup> deleteItems = new ArrayList<BuddyGroup>();
		for (String entry : entries) {
			for (BuddyGroup group : VikimgApp.buddyList) {
				for (BuddyEntry buddy : group.getBuddyList()) {
					if (buddy.getJid().equals(entry)) {
						group.getBuddyList().remove(buddy);
					}
				}
				if (group.getBuddyList().isEmpty()) {
					deleteItems.add(group);
				}
			}
		}
		for (BuddyGroup item : deleteItems) {
			VikimgApp.buddyList.remove(item);
		}

		setOnlineBuddyRatio();
		sortList();
		invalidateBuddyList();
		refreshBuddyList();

	}

	@Override
	public void entriesUpdated(Collection<String> entries) {
		Log.d("test", "updat");
		ArrayList<BuddyGroup> deleteItems = new ArrayList<BuddyGroup>();
		for (String entry : entries) {
			RosterEntry rEntry = VikimgApp.roster.getEntry(entry);

			String newGroup = null;
			Log.d("test", rEntry.getName());
			for (RosterGroup group : rEntry.getGroups()) {
				Log.d("test", group.getName());
				newGroup = group.getName();
			}
			if (newGroup != null) {

				BuddyGroup groupTo = null;
				for (BuddyGroup group : VikimgApp.buddyList) {
					if (group.getName().equals(newGroup)) {
						groupTo = group;
						break;
					}
				}
				if (groupTo != null) {
					BuddyEntry changedBuddy = null;
					for (BuddyEntry bEntry : groupTo.getBuddyList()) {
						if (bEntry.getJid().equals(entry)) {
							changedBuddy = bEntry;
						}
					}
					if (changedBuddy != null) {
						changedBuddy.setName(rEntry.getName());
					} else {
						groupTo.getBuddyList().add(
								new BuddyEntry(rEntry.getName(), rEntry.getUser(), VikimgApp.roster.getPresence(
										rEntry.getUser()).getStatus(), BuddyStatus.retrieveStatus(VikimgApp.roster
										.getPresence(rEntry.getUser()))));
					}

				} else {
					groupTo = new BuddyGroup(newGroup);
					groupTo.getBuddyList().add(
							new BuddyEntry(rEntry.getName(), rEntry.getUser(), VikimgApp.roster.getPresence(
									rEntry.getUser()).getStatus(), BuddyStatus.retrieveStatus(VikimgApp.roster
									.getPresence(rEntry.getUser()))));
					VikimgApp.buddyList.add(groupTo);
				}

			} else {
				for (BuddyGroup group : VikimgApp.buddyList) {
					for (BuddyEntry buddyEntry : group.getBuddyList()) {
						if (buddyEntry.getJid().equals(rEntry.getUser())) {
							Log.d("test", "del " + buddyEntry.getName());
							group.getBuddyList().remove(buddyEntry);
							if (group.getBuddyList().isEmpty()) {
								deleteItems.add(group);
							}
						}
					}
				}
				for (BuddyGroup item : deleteItems) {
					VikimgApp.buddyList.remove(item);
				}
			}
		}

		sortList();
		setOnlineBuddyRatio();
		invalidateBuddyList();
		refreshBuddyList();
	}

	@Override
	public void presenceChanged(Presence presence) {
		Log.d("test", "pres");
		String user = presence.getFrom();
		String description = presence.getStatus();
		String[] data = user.split("/");
		Presence bestPresence = VikimgApp.roster.getPresence(user);

		for (BuddyGroup group : VikimgApp.buddyList) {
			for (BuddyEntry entry : group.getBuddyList()) {
				if (entry.getJid().equals(data[0])) {
					entry.setStatus(BuddyStatus.retrieveStatus(bestPresence));
					if (description != null) {
						entry.setDescription(description);
					} else{
						entry.setDescription("");
					}
				}
			}
		}
		sortList();
		setOnlineBuddyRatio();
		refreshBuddyList();
	}

	private void refreshBuddyList() {
		VikimgApp.buddyListActivity.runOnUiThread(new Runnable() {
			public void run() {
				VikimgApp.buddyListAdapter.notifyDataSetChanged();
			}
		});
	}

	private void invalidateBuddyList() {
		VikimgApp.currentActivity.runOnUiThread(new Runnable() {
			public void run() {
				VikimgApp.buddyListAdapter.notifyDataSetInvalidated();
			}
		});
	}

	private void sortList() {
		Collections.sort(VikimgApp.buddyList, new BuddyGroupComparator());
		for (BuddyGroup group : VikimgApp.buddyList) {
			Collections.sort(group.getBuddyList(), new BuddyEntryComparator());
			Collections.sort(group.getBuddyList(), new BuddyStatusComparator());
		}
	}

	private void setOnlineBuddyRatio() {

		for (BuddyGroup group : VikimgApp.buddyList) {
			int online = 0;
			for (BuddyEntry entry : group.getBuddyList()) {
				if (entry.getStatus() != BuddyStatus.OFFLINE) {
					online++;
				}
				group.setOnlineRatio("(" + online + "/" + group.getBuddyList().size() + ")");
			}
		}
	}
}
