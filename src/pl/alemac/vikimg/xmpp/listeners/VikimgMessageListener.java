package pl.alemac.vikimg.xmpp.listeners;

import java.util.Date;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.util.StringUtils;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.message.MessageItem;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.util.Log;

public class VikimgMessageListener implements MessageListener {

	VikimgDatabaseHelper dbHelper = new VikimgDatabaseHelper(VikimgApp.currentActivity.getApplicationContext());

	@Override
	public void processMessage(Chat chat, Message message) {

		Log.d("message", message.getFrom());
		Log.d("message", message.getBody());
		
		
		boolean restartChat = true;
		PacketExtension ext = message.getExtension("jabber:x:delay");
		if (message.getType() == Message.Type.chat) {
			if (ext == null) {
				if (message.getBody() != null) {
					if (VikimgApp.audioManager.getRingerMode() != 1 && VikimgApp.isSound) {
						MediaPlayer mp = MediaPlayer.create(VikimgApp.currentActivity, R.raw.message_send);
						mp.start();
					}
					if (VikimgApp.isVibrate) {
						Vibrator v = (Vibrator) VikimgApp.currentActivity.getSystemService(Context.VIBRATOR_SERVICE);
						v.vibrate(300);
					}

					if (VikimgApp.inBackground) {
						VikimgViewUtils.showNotification();
					}

					for (ChatItem item : VikimgApp.chatList) {
						if (item.getChat().equals(chat)) {
							Log.d("chat", item.getParticipantName());
							Log.d("chat", item.getParticipantJid());
							item.getMessageList().add(
									new MessageItem(message.getBody(), item.getParticipantName(), new Date()));
							dbHelper.createMessage(VikimgDatabaseHelper.setMessageId(item.getParticipantJid()), item.getParticipantJid(), message.getBody(), new Date());
							if (VikimgApp.chatActivity != null) {
								refreshChatActivity();
							} else {
								item.setNewMessage(true);
								refreshChatList();
							}
							restartChat = false;
							break;
						}
					}
					if (restartChat) {
						restartChat(chat, message.getBody());
					}
				}
			}
		}
	}

	private void refreshChatActivity() {
		VikimgApp.chatActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				VikimgApp.messageListAdapter.notifyDataSetInvalidated();
			}
		});
	}

	private void refreshChatList() {
		VikimgApp.buddyListActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				VikimgApp.chatListAdapter.notifyDataSetInvalidated();
			}
		});
	}

	private void restartChat(Chat chat, String message) {
		String jid = StringUtils.parseBareAddress(chat.getParticipant());
		String name = null;

		RosterEntry rEntry = VikimgApp.roster.getEntry(jid);
		if (rEntry != null) {
			name = rEntry.getName();
		} else {
			name = jid;
		}
		Log.d("restart chat", jid);
		Log.d("restart chat", name);
		ChatItem item = new ChatItem(VikimgApp.user, jid, name, chat, true);
		item.getMessageList().add(new MessageItem(message, item.getParticipantName(), new Date()));
		dbHelper.createMessage(VikimgDatabaseHelper.setMessageId(item.getParticipantJid()), item.getParticipantJid(), message, new Date());
		VikimgApp.chatList.add(item);
		VikimgApp.buddyListActivity.runOnUiThread(new Runnable() {
			public void run() {
				VikimgApp.chatListAdapter.notifyDataSetInvalidated();
			}
		});
	}
}
