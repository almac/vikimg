package pl.alemac.vikimg.xmpp.listeners;

import org.jivesoftware.smack.ConnectionListener;

import pl.alemac.vikimg.VikimgApp;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;

public class VikimgConnectionListener implements ConnectionListener {

	@Override
	public void reconnectionSuccessful() {
		Log.d("connection", "Successfully reconnected to the XMPP server.");
		VikimgApp.isUserAvailable = true;
		refreshMenu();
	}

	@Override
	public void reconnectionFailed(Exception arg0) {
		Log.d("connection", "Failed to reconnect to the XMPP server.");
		VikimgApp.isUserAvailable = false;
		refreshMenu();
	}

	@Override
	public void reconnectingIn(int seconds) {
		Log.d("connection", "Reconnecting in " + seconds + " seconds.");
	}

	@Override
	public void connectionClosedOnError(Exception arg0) {
		Log.d("connection", "Connection to XMPP server was lost.");
		VikimgApp.isUserAvailable = false;
		refreshMenu();
	}

	@Override
	public void connectionClosed() {
		Log.d("connection", "XMPP connection was closed.");
		VikimgApp.isUserAvailable = false;
		refreshMenu();
	}
	
	private void refreshMenu() {
		VikimgApp.buddyListActivity.runOnUiThread(new Runnable() {
			public void run() {
				((SherlockFragmentActivity) VikimgApp.buddyListActivity).supportInvalidateOptionsMenu();
			}
		});
		
	}

}
