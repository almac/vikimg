package pl.alemac.vikimg.xmpp.listeners;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.util.StringUtils;

import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;

public class VikimgChatManagerListener implements ChatManagerListener {
	private VikimgDatabaseHelper dbHelper = new VikimgDatabaseHelper(VikimgApp.currentActivity.getApplicationContext());

	public void chatCreated(Chat chat, boolean locally) {

		if (!locally) {
			chat.addMessageListener(VikimgApp.messageListener);

			String jid = StringUtils.parseBareAddress(chat.getParticipant());
			String name = null;

			RosterEntry rEntry = VikimgApp.roster.getEntry(jid);
			if (rEntry != null && rEntry.getName() != null) {
				name = rEntry.getName();
			} else {
				name = jid;
			}
			VikimgApp.chatList.add(new ChatItem(VikimgApp.user, jid, name, chat));
			dbHelper.createArchiveBuddy(jid, name);
			VikimgApp.buddyListActivity.runOnUiThread(new Runnable() {
				public void run() {
					VikimgApp.chatListAdapter.notifyDataSetInvalidated();
				}
			});
		}
	}

}
