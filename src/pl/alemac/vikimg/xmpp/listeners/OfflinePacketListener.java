package pl.alemac.vikimg.xmpp.listeners;

import java.util.Date;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.packet.DelayInformation;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.message.MessageItem;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.util.Log;

public class OfflinePacketListener implements PacketListener {
 
	VikimgDatabaseHelper dbHelper = new VikimgDatabaseHelper(VikimgApp.currentActivity.getApplicationContext());
	@Override
	public void processPacket(Packet packet) {
		if (packet instanceof Message) {
			Message message = (Message) packet;
			Date date = null;
			PacketExtension ext = message.getExtension("jabber:x:delay");
			if (ext != null) {
				DelayInformation i = (DelayInformation) ext;
				date = i.getStamp();
				if (date == null) {
					date = new Date();
				}
				
				boolean noChat = true;
				String jid = StringUtils.parseBareAddress(packet.getFrom());

				for (ChatItem item : VikimgApp.chatList) {
					if (item.getParticipantJid().equals(jid)) {
						noChat = false;
						item.getMessageList().add(new MessageItem(message.getBody(), item.getParticipantName(), date));
						dbHelper.createMessage(VikimgDatabaseHelper.setMessageId(item.getParticipantJid()), item.getParticipantJid(), message.getBody(), new Date());
						item.setNewMessage(true);
					}
				}
				if (noChat) {
					String name = null;

					RosterEntry rEntry = VikimgApp.roster.getEntry(jid);
					if (rEntry != null) {
						name = rEntry.getName();
					} else {
						name = jid;
					}
					Log.d("offline", "wiadomosc offline");
					ChatItem item = new ChatItem(VikimgApp.user, jid, name, true);
					item.getMessageList().add(new MessageItem(message.getBody(), jid, date));
					VikimgApp.chatList.add(item);
					dbHelper.createArchiveBuddy(item.getParticipantJid(), item.getParticipantName());
					dbHelper.createMessage(VikimgDatabaseHelper.setMessageId(item.getParticipantJid()), item.getParticipantJid(), message.getBody(), new Date());
				}

				if (VikimgApp.inBackground) {
					VikimgViewUtils.showNotification();
				}
				if (VikimgApp.audioManager.getRingerMode() != 1 && VikimgApp.isSound) {
					MediaPlayer mp = MediaPlayer.create(VikimgApp.currentActivity, R.raw.message_send);
					mp.setVolume(1.0f, 1.0f);
					mp.start();
				}
				if (VikimgApp.isVibrate) {
					Vibrator v = (Vibrator) VikimgApp.currentActivity.getSystemService(Context.VIBRATOR_SERVICE);
					v.vibrate(300);
				}
			}
		}
	}
}
