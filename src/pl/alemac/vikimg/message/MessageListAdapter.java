package pl.alemac.vikimg.message;

import java.util.ArrayList;

import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.utilities.VikimgTimeUtils;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MessageListAdapter extends ArrayAdapter<MessageItem> {

	private int resource;
	private LayoutInflater inflater;

	public MessageListAdapter(Context _context, int textViewResourceId, ArrayList<MessageItem> objects) {
		super(_context, textViewResourceId, objects);
		resource = textViewResourceId;
		inflater = LayoutInflater.from(_context);
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		MessageItem item = getItem(position);
		MessageListViewCache viewCache;

		if (convertView == null) {
			convertView = (RelativeLayout) inflater.inflate(resource, null);
			viewCache = new MessageListViewCache(convertView);
			convertView.setTag(viewCache);
		} else {
			convertView = (RelativeLayout) convertView;
			viewCache = (MessageListViewCache) convertView.getTag();
		}

		TextView message = viewCache.getMessage();
		message.setText(item.getMessage());

		TextView sender = viewCache.getSender();

		if (item.getSender().equals(VikimgApp.user)) {
			sender.setTextColor(Color.parseColor("#30b712"));
		} else {
			sender.setTextColor(Color.parseColor("#128bb7"));
		}
		sender.setText(item.getSender());

		TextView time = viewCache.getTime();
		time.setText(VikimgTimeUtils.getTime(item.getDate()));

		TextView date = viewCache.getDate();
		date.setText(VikimgTimeUtils.getDate(item.getDate()));

		return convertView;
	}

}
