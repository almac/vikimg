package pl.alemac.vikimg.message;


import pl.alemac.vikimg.R;
import android.view.View;
import android.widget.TextView;

public class MessageListViewCache {

	private View baseView;
	private TextView message;
	private TextView sender;
	private TextView time;
	private TextView date;

	public MessageListViewCache(View baseView) {
		this.baseView = baseView;
	}
	
	public TextView getMessage() {
		if (message == null) {
			message = (TextView) baseView.findViewById(R.id.message);
		}
		return message;
	}
	
	public TextView getSender() {
		if (sender == null) {
			sender = (TextView) baseView.findViewById(R.id.sender);
		}
		return sender;
	}
	
	public TextView getTime() {
		if (time == null) {
			time = (TextView) baseView.findViewById(R.id.time);
		}
		return time;
	}
	
	public TextView getDate() {
		if (date == null) {
			date = (TextView) baseView.findViewById(R.id.date);
		}
		return date;
	}

}
