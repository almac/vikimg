package pl.alemac.vikimg.message;

import java.util.Date;

public class MessageItem  {

	private String message;
	private String sender;
	private Date date;

	public MessageItem(String message, String sender, Date date) {
		super();
		this.message = message;
		this.sender = sender;
		this.date = date;
	}

	public MessageItem() {
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
