package pl.alemac.vikimg;

import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.database.model.Account;
import pl.alemac.vikimg.database.model.XmppAccount;
import pl.alemac.vikimg.service.VikimgService;
import pl.alemac.vikimg.utilities.VikimgCryptoUtils;
import pl.alemac.vikimg.utilities.VikimgLoginUtils;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private UserLoginTask userLoginTask = null;

	private String nick;
	private String password;
	private String jid;
	private String xmppPassword;

	private EditText nickView;
	private EditText passwordView;
	private View loginFormView;
	private View loginStatusView;
	private TextView loginStatusMessageView;
	private Context context;
	private VikimgDatabaseHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		VikimgApp.currentActivity = this;
		dbHelper = new VikimgDatabaseHelper(this);


//		final Context inn = this;
//		BroadcastReceiver onNotice = new BroadcastReceiver() {
//
//			@Override
//			public void onReceive(Context context, Intent intent) {
//
//				if (intent.getAction().equals(VikimgService.REACTION)) {
//					Log.d("test", "onReceive called REACTION");
//					Log.d("test", intent.getStringExtra("user"));
//					Intent in = new Intent(inn, VikimgService.class);
//					in.setAction(VikimgService.REREACTION);
//					in.putExtra("user", "login_worked");
//					Log.d("test", "sending broadcast REREACTION");
//					startService(in);
//				}
//			}
//		};
//		
//		IntentFilter iff = new IntentFilter(VikimgService.REACTION);
//		LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, iff);
//		

		setContentView(R.layout.activity_login);
		nickView = (EditText) findViewById(R.id.account_nick_login);
		nickView.setFocusable(true);
		passwordView = (EditText) findViewById(R.id.password);
		passwordView.setFocusable(true);
		passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					attemptLogin();
					return true;
				}
				return false;
			}
		});
		loginFormView = findViewById(R.id.login_form);
		loginStatusView = findViewById(R.id.login_status);
		loginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptLogin();
			}
		});

		findViewById(R.id.new_account).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				createNewAccount();

			}
		});

	}

	private void createNewAccount() {
		Intent intent = new Intent(LoginActivity.this, NewAccountActivity.class);
		LoginActivity.this.startActivity(intent);
		finish();
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	private void attemptLogin() {
		if (userLoginTask != null) {
			return;
		}

		nickView.setError(null);
		passwordView.setError(null);

		nick = nickView.getText().toString();
		password = passwordView.getText().toString();

		int result = prepareLoginData();

		boolean cancel = false;
		View focusView = null;

		if (TextUtils.isEmpty(password)) {
			passwordView.setError(getString(R.string.error_field_required));
			focusView = passwordView;
			cancel = true;
		} else if (result == 2) {
			passwordView.setError(getString(R.string.error_incorrect_password));
			focusView = passwordView;
			cancel = true;
		} else if (result == 3) {
			cancel = true;
			VikimgViewUtils.showErrorDialog(VikimgApp.currentActivity,
					getResources().getString(R.string.error_identifying));

		}

		if (TextUtils.isEmpty(nick)) {
			nickView.setError(getString(R.string.error_field_required));
			focusView = nickView;
			cancel = true;
		} else if (result == 1) {
			nickView.setError(getString(R.string.error_invalid_nick));
			focusView = nickView;
			cancel = true;
		}

		if (cancel) {
			if (focusView != null) {
				focusView.requestFocus();
			}

		} else {

			initializeSettings();

			loginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			userLoginTask = new UserLoginTask();
			userLoginTask.execute((Void) null);
		}
	}

	private void initializeSettings() {
		SharedPreferences settings = getSharedPreferences(VikimgApp.user + "_" + VikimgApp.SETTINGS_PREF, 0);
		VikimgApp.isSound = settings.getBoolean("sound_preference", true);
		VikimgApp.isVibrate = settings.getBoolean("vibration_preference", true);

		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("sound_preference", VikimgApp.isSound);
		editor.putBoolean("vibration_preference", VikimgApp.isVibrate);
		editor.commit();
		VikimgApp.audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		VikimgApp.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 15, 0);

	}

	private int prepareLoginData() {
		// get data from database
		Account account = new Account();
		XmppAccount xmppAccount = new XmppAccount();

		account = dbHelper.getAccountByNick(nick);
		if (account == null) {
			return 1;
		} else {
			xmppAccount = dbHelper.getXmppAccountByNick(nick);
		}

		String hash = VikimgCryptoUtils.getPassword(password, account.getPassword().substring(0, 11));

		// Log.d("crypto", "Random Salt: "+VikimgCryptoUtils.getSalt());
		// Log.d("crypto", "Salt: "+account.getPassword().substring(0, 11));
		// Log.d("crypto", "Hash: "+account.getPassword().substring(11));
		// Log.d("crypto", "All: "+account.getPassword());
		// Log.d("crypto", "Hash2: "+hash);

		if (!hash.equals(account.getPassword().substring(11))) {
			return 2;
		}

		String seed = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

		VikimgApp.user = account.getNick();
		xmppPassword = new String();
		jid = xmppAccount.getJid();

		try {
			xmppPassword = VikimgCryptoUtils.decrypt(seed, xmppAccount.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
			return 3;
		}
		dbHelper.closeDB();
		return 0;

	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			loginStatusView.setVisibility(View.VISIBLE);
			loginStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							loginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
						}
					});

			loginFormView.setVisibility(View.VISIBLE);
			loginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
						}
					});
		} else {
			loginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	public class UserLoginTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			Intent i = new Intent(getApplicationContext(), VikimgService.class);
			i.setAction(VikimgService.START);
			startService(i);
			return VikimgLoginUtils.setUpConnection(LoginActivity.this, jid, xmppPassword);
		}

		@Override
		protected void onPostExecute(final String results) {
			userLoginTask = null;
			int result = Integer.parseInt(results);

			if (result == 0) {

				Intent intent = new Intent(LoginActivity.this, MainScreen.class);
				LoginActivity.this.startActivity(intent);
				showProgress(false);
				finish();

			} else if (result == 1) {
				Intent intent = new Intent(LoginActivity.this, MainScreen.class);
				VikimgApp.isUserAvailable = false;
				LoginActivity.this.startActivity(intent);
				showProgress(false);
				Toast.makeText(context, getResources().getString(R.string.no_connection), Toast.LENGTH_LONG).show();
				finish();

			} else if (result == 2) {
				Intent intent = new Intent(LoginActivity.this, MainScreen.class);
				VikimgApp.isUserAvailable = false;
				LoginActivity.this.startActivity(intent);
				showProgress(false);
				Toast.makeText(context, getResources().getString(R.string.error_incorrect_xmpp_password),
						Toast.LENGTH_LONG).show();
				finish();
			}
		}

		@Override
		protected void onCancelled() {
			userLoginTask = null;
			showProgress(false);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		password = null;
		xmppPassword = null;
	}

}
