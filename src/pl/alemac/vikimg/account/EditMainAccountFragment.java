package pl.alemac.vikimg.account;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.utilities.VikimgCryptoUtils;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockFragment;

public class EditMainAccountFragment extends SherlockFragment {

	private EditText newPassword;
	private EditText reNewPassword;
	private Button okButton;
	private Button cancelButton;
	private VikimgDatabaseHelper dbHelper;

	public static SherlockFragment newInstance() {
		EditMainAccountFragment mFrgment = new EditMainAccountFragment();
		return mFrgment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.edit_main_account_fragment, null);
		return v;
	}

	public void onStart() {
		super.onStart();

		newPassword = (EditText) getActivity().findViewById(R.id.new_main_account_pwd);
		reNewPassword = (EditText) getActivity().findViewById(R.id.re_main_account_pwd);

		cancelButton = (Button) getActivity().findViewById(R.id.edit_main_account_cancel);
		okButton = (Button) getActivity().findViewById(R.id.edit_main_account_ok);

		dbHelper = new VikimgDatabaseHelper(getActivity());
		setListeners();
	}

	private void setListeners() {
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onOkClicked();
			}
		});

		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finishSteps();
			}

		});

	}

	private void finishSteps() {

		SherlockFragment accountFragment = AccountFragment.newInstance();
		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.account_fragment_container, accountFragment, "accountFragment").commit();

	}

	private void onOkClicked() {

		newPassword.setError(null);

		String newPass = newPassword.getText().toString();
		String reNewPass = reNewPassword.getText().toString();

		if (newPass.length() < 5) {

			newPassword.setError(getResources().getString(R.string.error_invalid_password));
			newPassword.requestFocus();
		}

		if (!newPass.equals(reNewPass)) {
			newPassword.setError(getResources().getString(R.string.error_password_missmatch));
			newPassword.requestFocus();

		} else {
			String accountSalt = VikimgCryptoUtils.getSalt();
			String md5hash = VikimgCryptoUtils.getPassword(newPass, accountSalt);
			dbHelper.updateAccountPassword(accountSalt + md5hash);

			final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
			b.setMessage(R.string.change_password_success);
			b.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					finishSteps();
				}
			});

			b.show();
		}

	}

}
