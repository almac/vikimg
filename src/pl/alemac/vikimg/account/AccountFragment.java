package pl.alemac.vikimg.account;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.database.model.XmppAccount;
import pl.alemac.vikimg.utilities.VikimgCryptoUtils;
import pl.alemac.vikimg.utilities.VikimgLoginUtils;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class AccountFragment extends SherlockFragment {

	private TextView mainAccountView;
	private TextView xmppAccountJid;
	private ImageView xmppAccountState;
	private RelativeLayout xmppAccountLayout;
	private VikimgDatabaseHelper dbHelper;
	private XmppAccount xmppAccount;

	public static SherlockFragment newInstance() {
		AccountFragment mFrgment = new AccountFragment();
		return mFrgment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.account_fragment, null);
		return v;
	}

	public void onStart() {
		super.onStart();
		
		dbHelper = new VikimgDatabaseHelper(getActivity());

		mainAccountView = (TextView) getActivity().findViewById(R.id.main_account);
		xmppAccountLayout = (RelativeLayout) getActivity().findViewById(R.id.layout_xmpp_account);
		xmppAccountJid = (TextView) getActivity().findViewById(R.id.jid_xmpp_account);
		xmppAccountState = (ImageView) getActivity().findViewById(R.id.state_xmpp_account);

		mainAccountView.setText(VikimgApp.user);
		xmppAccount = dbHelper.getXmppAccountByNick(VikimgApp.user);
		dbHelper.closeDB();
		xmppAccountJid.setText(xmppAccount.getJid());

		if (VikimgApp.isUserAvailable) {
			xmppAccountState.setImageResource(R.drawable.user_connected);
		} else {
			xmppAccountState.setImageResource(R.drawable.user_disconnected);
		}

		setListeners();
	}

	private void setListeners() {

		mainAccountView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Dialog dialog = createMainAccountOptionDialog();
				dialog.show();
			}

		});

		xmppAccountLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Dialog dialog = createXmppAccountOptionDialog();
				dialog.show();
			}
		});

	}

	public Dialog createMainAccountOptionDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(VikimgApp.user).setItems(R.array.main_account_dialog_array,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int position) {
						if (position == 0) {
							dialog.dismiss();
							SherlockFragment editMainAccountFragment = EditMainAccountFragment.newInstance();
							getActivity()
									.getSupportFragmentManager()
									.beginTransaction()
									.replace(R.id.account_fragment_container, editMainAccountFragment,
											"editMainAccountFragment").commit();
						} else {
							dialog.dismiss();
							Dialog removalDialog = removeArchiveDialog();
							removalDialog.show();
						}
					}
				});

		return builder.create();
	}

	private Dialog removeArchiveDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getResources().getString(R.string.delete_archive_confirm));
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				new CleanArchiveTask().execute();
			}
		});
		builder.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});

		return builder.create();
	}

	public Dialog createXmppAccountOptionDialog() {
		String edit = getResources().getString(R.string.edit_account);
		String reconnect = new String();
		if (VikimgApp.isUserAvailable) {
			reconnect = getResources().getString(R.string.disconnect);
		} else {
			reconnect = getResources().getString(R.string.connect);
		}

		String[] options = { reconnect, edit };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(xmppAccount.getJid()).setItems(options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int position) {
				if (position == 1) {
					dialog.dismiss();
					SherlockFragment editXmppAccountFragment = EditXmppAccountFragment.newInstance();
					Bundle bundle = new Bundle();
					bundle.putString("xmppAccountJid", xmppAccount.getJid());
					editXmppAccountFragment.setArguments(bundle);
					getActivity()
							.getSupportFragmentManager()
							.beginTransaction()
							.replace(R.id.account_fragment_container, editXmppAccountFragment,
									"editXmppAccountFragment").commit();
				} else {
					// connect task
					dialog.dismiss();

					new LoginTask().execute();
				}
			}
		});

		return builder.create();
	}

	private class CleanArchiveTask extends AsyncTask<String, String, String> {
		private ProgressDialog dialog;

		public CleanArchiveTask() {
			dialog = new ProgressDialog(getActivity());
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getActivity().getResources().getString(R.string.progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			dbHelper.deleteAllMessages();
			return null;
		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);
			dialog.dismiss();
		}

	}

	private class LoginTask extends AsyncTask<String, String, String> {
		private ProgressDialog dialog;

		public LoginTask() {
			dialog = new ProgressDialog(getActivity());
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getActivity().getResources().getString(R.string.progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			if (!VikimgApp.isUserAvailable) {
				VikimgApp.buddyList.clear();
				if (VikimgApp.connection.isConnected()) {

					return VikimgLoginUtils.differentConnect();
				} else {
					String xmppPassword = new String();
					String seed = Secure.getString(getActivity().getContentResolver(), Secure.ANDROID_ID);
					try {
						xmppPassword = VikimgCryptoUtils.decrypt(seed, xmppAccount.getPassword());
					} catch (Exception e) {
						e.printStackTrace();
						return "3";
					}
					return VikimgLoginUtils.setUpConnection(getActivity(), xmppAccount.getJid() , xmppPassword);
				}

			} else {
				return VikimgLoginUtils.logout();
			}

		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);
			dialog.dismiss();

			int result = Integer.parseInt(results);

			if (result == 1) {
				Log.d("ccc", "Blad polaczenia");
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(getActivity(), getResources().getString(R.string.no_connection));
			} else if (result == 2) {
				Log.d("ccc", getResources().getString(R.string.error_incorrect_xmpp_password));
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(getActivity(),
						getResources().getString(R.string.error_incorrect_xmpp_password));
			} else if (result == 0) {
				if (VikimgApp.isUserAvailable) {
					xmppAccountState.setImageResource(R.drawable.user_connected);
				} else {
					xmppAccountState.setImageResource(R.drawable.user_disconnected);
				}

			} else if (result == 20) {
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(getActivity(), getResources().getString(R.string.error_unknown));
			} else if (result == 10) {
				dialog.dismiss();
				if (VikimgApp.isUserAvailable) {
					xmppAccountState.setImageResource(R.drawable.user_connected);
				} else {
					xmppAccountState.setImageResource(R.drawable.user_disconnected);
				}
			}
		}

	}

}
