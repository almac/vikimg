package pl.alemac.vikimg.account;

import java.util.ArrayList;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.buddy.BuddyGroup;
import pl.alemac.vikimg.chatList.ChatItem;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.utilities.VikimgCryptoUtils;
import pl.alemac.vikimg.utilities.VikimgLoginUtils;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

public class EditXmppAccountFragment extends SherlockFragment {

	private EditText newJid;
	private EditText newPassword;
	private Button saveButton;
	private Button cancelButton;
	private String oldJidString;
	private VikimgDatabaseHelper dbHelper;

	public static SherlockFragment newInstance() {
		EditXmppAccountFragment mFrgment = new EditXmppAccountFragment();
		return mFrgment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.edit_xmpp_account_fragment, null);
		return v;
	}

	public void onStart() {
		super.onStart();

		newJid = (EditText) getActivity().findViewById(R.id.new_xmpp_account_jid);
		newPassword = (EditText) getActivity().findViewById(R.id.new_xmpp_account_pwd);

		Bundle bundle = getArguments();
		if (bundle != null) {
			oldJidString = bundle.getString("xmppAccountJid");
			newJid.setText(oldJidString);
		}

		cancelButton = (Button) getActivity().findViewById(R.id.edit_xmpp_account_cancel);
		saveButton = (Button) getActivity().findViewById(R.id.edit_xmpp_account_ok);

		if (VikimgApp.isUserAvailable) {
			Toast.makeText(getActivity(), R.string.change_login_data, Toast.LENGTH_LONG).show();
			newJid.setEnabled(false);
			newPassword.setEnabled(false);
			saveButton.setEnabled(false);
		}

		dbHelper = new VikimgDatabaseHelper(getActivity());
		setListeners();
	}

	private void onSaveClicked() {

		newJid.setError(null);
		newPassword.setError(null);

		String newJidString = newJid.getText().toString();
		String newPasswordString = newPassword.getText().toString();

		if (!newJidString.contains("@")) {
			newJid.setError(getResources().getString(R.string.error_invalid_jid));
			newJid.requestFocus();
			return;
		} else if (TextUtils.isEmpty(newJidString)) {
			newJid.setError(getResources().getString(R.string.error_field_required));
			newJid.requestFocus();
			return;
		}
		if (TextUtils.isEmpty(newPasswordString)) {
			newPassword.setError(getResources().getString(R.string.error_field_required));
			newPassword.requestFocus();
			return;
		}

		new ConnectTask(newJidString, newPasswordString).execute();

	}

	private void setListeners() {
		saveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onSaveClicked();
			}
		});

		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finishSteps();
			}
		});

	}

	private void finishSteps() {

		SherlockFragment accountFragment = AccountFragment.newInstance();
		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.account_fragment_container, accountFragment, "accountFragment").commit();

	}

	private class ConnectTask extends AsyncTask<String, String, String> {
		private ProgressDialog dialog;
		private String jid;
		private String pwd;

		public ConnectTask(String jid, String pwd) {
			dialog = new ProgressDialog(getActivity());
			this.jid = jid;
			this.pwd = pwd;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getActivity().getResources().getString(R.string.progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {

			VikimgApp.chatList = new ArrayList<ChatItem>();
			VikimgApp.buddyList = new ArrayList<BuddyGroup>();

			if (VikimgApp.connection != null && VikimgApp.connection.isConnected()) {

				if (VikimgApp.roster != null) {
					VikimgApp.roster.removeRosterListener(VikimgApp.rosterListener);
					VikimgApp.rosterListener = null;
					VikimgApp.roster = null;
				}

				VikimgApp.connection.removeConnectionListener(VikimgApp.connectionListener);
				VikimgApp.connectionListener = null;

				VikimgApp.connection.disconnect();
				VikimgApp.connection = null;

				VikimgApp.chatListAdapter = null;
				VikimgApp.buddyListAdapter = null;
				VikimgApp.messageListAdapter = null;
			}

			String salt = Secure.getString(getActivity().getContentResolver(), Secure.ANDROID_ID);
			String encryptedPwd = null;
			try {
				encryptedPwd = VikimgCryptoUtils.encrypt(salt, pwd);
			} catch (Exception e) {
				e.printStackTrace();
				return "10";
			}
			try {
				dbHelper.updateXmppAccount(oldJidString, jid, encryptedPwd);
			} catch (Exception e2) {
				e2.printStackTrace();
				return "10";
			}

			return VikimgLoginUtils.setUpConnection(getActivity(), jid, pwd);
		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);

			int result = Integer.parseInt(results);

			if (result == 1) {
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(getActivity(), getResources().getString(R.string.no_connection));
			} else if (result == 2) {
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(getActivity(),
						getResources().getString(R.string.error_incorrect_xmpp_password));
			} else if (result == 0) {
				dialog.dismiss();			
				
				final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
				b.setMessage(R.string.login_success);
				b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finishSteps();
					}
				});

				b.show();
				
			} else if (result == 10) {
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(getActivity(), getResources().getString(R.string.error_unknown));
			}
		}

	}

}