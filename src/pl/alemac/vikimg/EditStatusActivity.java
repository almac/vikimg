package pl.alemac.vikimg;

import org.jivesoftware.smack.packet.Presence;

import pl.alemac.vikimg.buddy.BuddyEntry;
import pl.alemac.vikimg.buddy.BuddyGroup;
import pl.alemac.vikimg.buddy.BuddyStatus;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import pl.alemac.vikimg.database.model.XmppAccount;
import pl.alemac.vikimg.utilities.VikimgCryptoUtils;
import pl.alemac.vikimg.utilities.VikimgLoginUtils;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class EditStatusActivity extends SherlockActivity {
	private ActionBar mActionBar;
	private Spinner statusSpinner;
	private Button okButton;
	private Button cancelButton;
	private EditText description;
	private Presence presence;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		VikimgApp.currentActivity = this;
		VikimgViewUtils.showRunningNotification(this);
		setContentView(R.layout.activity_edit_status);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle("Edycja statusu");

		SharedPreferences settings = getSharedPreferences(VikimgApp.user + "_" + VikimgApp.STATUS_PREF, 0);
		String desc = settings.getString("description", "default");
		int status = settings.getInt("status", -1);

		statusSpinner = (Spinner) findViewById(R.id.status_spinner);
		StatusAdapter statusListAdapter = null;
		statusListAdapter = new StatusAdapter(this, R.layout.row_status_spinner, BuddyStatus.values());

		statusSpinner.setAdapter(statusListAdapter);

		if (status != -1) {
			statusSpinner.setSelection(status);
		}

		description = (EditText) findViewById(R.id.descriptionStatus);
		if (!desc.equals("default")) {
			description.setText(desc);
		}

		okButton = (Button) findViewById(R.id.okStatus);
		cancelButton = (Button) findViewById(R.id.cancelStatus);

		presence = new Presence(Presence.Type.available);

		setListeners();

	}

	private void setListeners() {
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(EditStatusActivity.this, MainScreen.class));
				finish();
			}
		});

		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onOkButtonAction();
			}
		});
	}

	private void onOkButtonAction() {
		BuddyStatus status = (BuddyStatus) statusSpinner.getSelectedItem();
		String descriptionString = description.getText().toString();

		if (VikimgApp.connection.isConnected() && VikimgApp.connection.isAuthenticated()) {
			SharedPreferences settings = getSharedPreferences(VikimgApp.user + "_" + VikimgApp.STATUS_PREF, 0);
			SharedPreferences.Editor editor = settings.edit();

			if (description != null) {
				presence.setStatus(descriptionString);
				editor.putString("description", descriptionString);
				editor.commit();
			}

			if (statusSpinner.getSelectedItem() != null) {
				if (status.equals(BuddyStatus.OFFLINE)) {
					VikimgApp.isUserAvailable = false;
					setOfflineBuddyList();
				} else {
					VikimgApp.isUserAvailable = true;
					presence.setMode(BuddyStatus.retrievePresence(status));
				}
			}

			if (VikimgApp.isUserAvailable) {
				VikimgApp.connection.sendPacket(presence);
				editor.putInt("status", status.ordinal());
				editor.commit();
			} else {
				VikimgLoginUtils.logout();
			}
			startActivity(new Intent(EditStatusActivity.this, MainScreen.class));
			finish();

		} else {
			if (statusSpinner.getSelectedItem() != null) {
				if (!status.equals(BuddyStatus.OFFLINE)) {
					SharedPreferences settings = getSharedPreferences(VikimgApp.user + "_" + VikimgApp.STATUS_PREF, 0);
					SharedPreferences.Editor editor = settings.edit();

					editor.putString("description", descriptionString);
					editor.putInt("status", status.ordinal());
					editor.commit();

					LoginTask task = new LoginTask();
					task.execute();
				} else {
					startActivity(new Intent(EditStatusActivity.this, MainScreen.class));
					finish();
				}
			}
		}
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		startActivity(new Intent(EditStatusActivity.this, MainScreen.class));
		finish();
		return true;
	}

	public class StatusAdapter extends ArrayAdapter<BuddyStatus> {

		private BuddyStatus[] buddyStatus;

		public StatusAdapter(Context context, int textViewResourceId, BuddyStatus[] buddyStatus) {
			super(context, textViewResourceId, buddyStatus);

			this.buddyStatus = buddyStatus;
		}

		@Override
		public View getDropDownView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		public View getCustomView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.row_status_spinner, parent, false);
			TextView label = (TextView) row.findViewById(R.id.status_name);
			if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB_MR2) {
				label.setTextColor(Color.parseColor("#000000"));
			}
			label.setText(buddyStatus[position].getName());

			ImageView icon = (ImageView) row.findViewById(R.id.status_icon);
			icon.setImageResource(buddyStatus[position].getResource());

			return row;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(EditStatusActivity.this, MainScreen.class));
			finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

	private void setOfflineBuddyList() {
		for (BuddyGroup group : VikimgApp.buddyList) {
			for (BuddyEntry entry : group.getBuddyList()) {
				entry.setStatus(BuddyStatus.OFFLINE);
			}
			group.setOnlineRatio("(0/" + group.getBuddyList().size() + ")");
		}
	}

	private class LoginTask extends AsyncTask<String, String, String> {
		private ProgressDialog dialog;
		private VikimgDatabaseHelper dbHelper;

		public LoginTask() {
			dialog = new ProgressDialog(EditStatusActivity.this);
			dbHelper = new VikimgDatabaseHelper(EditStatusActivity.this);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getResources().getString(R.string.progress));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			VikimgApp.buddyList.clear();
			XmppAccount xmppAccount = dbHelper.getXmppAccountByNick(VikimgApp.user);
			dbHelper.closeDB();
			String xmppPassword = new String();
			String seed = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
			try {
				xmppPassword = VikimgCryptoUtils.decrypt(seed, xmppAccount.getPassword());
			} catch (Exception e) {
				e.printStackTrace();
				return "20";
			}
			return VikimgLoginUtils.setUpConnection(EditStatusActivity.this, xmppAccount.getJid(), xmppPassword);

		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);
			dialog.dismiss();

			int result = Integer.parseInt(results);

			if (result == 1) {
				Log.d("ccc", "Blad polaczenia");
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(EditStatusActivity.this,
						getResources().getString(R.string.no_connection));
			} else if (result == 2) {
				Log.d("ccc", getResources().getString(R.string.error_incorrect_xmpp_password));
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(EditStatusActivity.this,
						getResources().getString(R.string.error_incorrect_xmpp_password));

			} else if (result == 20) {
				dialog.dismiss();
				VikimgViewUtils.showErrorDialog(EditStatusActivity.this,
						getResources().getString(R.string.error_unknown));
			} else if (result == 0) {
				startActivity(new Intent(EditStatusActivity.this, MainScreen.class));
				finish();
			}
		}

	}

}
