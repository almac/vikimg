package pl.alemac.vikimg.settings;

import pl.alemac.vikimg.AboutActivity;
import pl.alemac.vikimg.MainScreen;
import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.utilities.VikimgViewUtils;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;

public class SettingsActivity extends SherlockPreferenceActivity implements OnSharedPreferenceChangeListener, OnPreferenceClickListener {

	private ActionBar mActionBar;

	@SuppressWarnings({ "deprecation" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		VikimgApp.currentActivity = this;
		VikimgViewUtils.showRunningNotification(this);
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setTitle("Ustawienia");

		getListView().setBackgroundColor(Color.TRANSPARENT);
		getListView().setCacheColorHint(Color.TRANSPARENT);
		getListView().setBackgroundResource(R.drawable.background);

		PreferenceManager prefMgr = getPreferenceManager();
		prefMgr.setSharedPreferencesName(VikimgApp.user + "_" + VikimgApp.SETTINGS_PREF);
		prefMgr.getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
//		((Preference) prefMgr.getSharedPreferences()).setOnPreferenceClickListener(this);
		
		addPreferencesFromResource(R.xml.preferences);
//		Preference connectToNewComputer= findPreference("connectToNewComputer");
//		this.setOnPreferenceClickListener(this);
		findPreference("about").setOnPreferenceClickListener(this);
		

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		startActivity(new Intent(SettingsActivity.this, MainScreen.class));
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(SettingsActivity.this, MainScreen.class));
			finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		VikimgViewUtils.setInBackground(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		sharedPreferences = getSharedPreferences(VikimgApp.user + "_" + VikimgApp.SETTINGS_PREF, 0);
		if (key.equals("sound_preference")) {
			Log.d("pref", "sound: " + sharedPreferences.getBoolean(key, true));
			VikimgApp.isSound = sharedPreferences.getBoolean(key, true);

		} else if (key.equals("vibration_preference")) {
			Log.d("pref", "vibration: " + sharedPreferences.getBoolean(key, true));
			VikimgApp.isVibrate = sharedPreferences.getBoolean(key, true);
		}

	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		Log.d("pref", preference.getKey());
		if(preference.getKey().equals("about")){
			Intent intent = new Intent(SettingsActivity.this, AboutActivity.class);
			startActivity(intent);
			finish();
		}
		
		return false;
	}
}
