package pl.alemac.vikimg.archive;

import java.util.ArrayList;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.archive.model.BuddyArchiveBinder;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.actionbarsherlock.app.SherlockFragment;

public class BuddyArchiveFragment extends SherlockFragment {

	private ExpandableListView archiveListView;
	private ArrayList<BuddyArchiveBinder> buddyArchiveBinderList;
	private ArchiveExpandableListAdapter adapter;
	private VikimgDatabaseHelper dbHelper;

	public static SherlockFragment newInstance() {
		BuddyArchiveFragment mFrgment = new BuddyArchiveFragment();
		return mFrgment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.buddy_archive_fragment, null);
		return v;
	}

	public void onStart() {
		super.onStart();

		dbHelper = new VikimgDatabaseHelper(getActivity());
		buddyArchiveBinderList = new ArrayList<BuddyArchiveBinder>();
		archiveListView = (ExpandableListView) getActivity().findViewById(R.id.archive_list);
		Log.e("database", "test");
		buddyArchiveBinderList = dbHelper.getDistinctArchiveBuddy();
		adapter = new ArchiveExpandableListAdapter(getActivity(), buddyArchiveBinderList);
		archiveListView.setAdapter(adapter);
		dbHelper.closeDB();

		archiveListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				String jid = buddyArchiveBinderList.get(groupPosition).getBuddyJid();
				String date = buddyArchiveBinderList.get(groupPosition).getArchiveDate().get(childPosition);

				new ChatArchiveTask(jid, date).execute();
				return false;
			}

		});
	}

	private class ChatArchiveTask extends AsyncTask<String, String, String> {

		private ProgressDialog dialog;
		private String jid;
		private String date;

		public ChatArchiveTask(String jid, String date) {
			dialog = new ProgressDialog(getActivity());
			this.jid = jid;
			this.date = date;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(getActivity().getResources().getString(R.string.data_loading));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {

			VikimgApp.archiveMessageList = dbHelper.getSpecficMessages(jid, date);
			dbHelper.closeDB();
			return null;

		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);
			dialog.dismiss();
			SherlockFragment chatArchiveFragment = ChatArchiveFragment.newInstance();
			getActivity().getSupportFragmentManager().beginTransaction()
					.replace(R.id.frag_containter, chatArchiveFragment, "chatArchiveFragment").commit();
		}

	}

}
