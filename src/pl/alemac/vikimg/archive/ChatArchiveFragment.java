package pl.alemac.vikimg.archive;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.VikimgApp;
import pl.alemac.vikimg.message.MessageListAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;

public class ChatArchiveFragment extends SherlockFragment {

	private ListView chatListView;
	private MessageListAdapter adapter;

	public static SherlockFragment newInstance() {
		ChatArchiveFragment mFrgment = new ChatArchiveFragment();
		return mFrgment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.chat_archive_fragment, null);
		return v;
	}

	public void onStart() {
		super.onStart();

		chatListView = (ListView) getActivity().findViewById(R.id.message_list);
		adapter = new MessageListAdapter(getActivity(), R.layout.chat_message_row, VikimgApp.archiveMessageList);
		chatListView.setAdapter(adapter);
	}
}
