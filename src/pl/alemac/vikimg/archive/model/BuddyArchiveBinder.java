package pl.alemac.vikimg.archive.model;

import java.util.ArrayList;

public class BuddyArchiveBinder {
	private String buddyNick;
	private String buddyJid;
	private ArrayList<String> archiveDate;

	public BuddyArchiveBinder(String buddyNick, String buddyJid) {
		this.buddyNick = buddyNick;
		this.buddyJid = buddyJid;
		archiveDate = new ArrayList<String>();
	}

	public String getBuddyNick() {
		return buddyNick;
	}

	public void setBuddyNick(String buddyNick) {
		this.buddyNick = buddyNick;
	}

	public ArrayList<String> getArchiveDate() {
		return archiveDate;
	}

	public void setArchiveDate(ArrayList<String> archiveDate) {
		this.archiveDate = archiveDate;
	}

	public String getBuddyJid() {
		return buddyJid;
	}

	public void setBuddyJid(String buddyJid) {
		this.buddyJid = buddyJid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((archiveDate == null) ? 0 : archiveDate.hashCode());
		result = prime * result + ((buddyJid == null) ? 0 : buddyJid.hashCode());
		result = prime * result + ((buddyNick == null) ? 0 : buddyNick.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuddyArchiveBinder other = (BuddyArchiveBinder) obj;
		if (archiveDate == null) {
			if (other.archiveDate != null)
				return false;
		} else if (!archiveDate.equals(other.archiveDate))
			return false;
		if (buddyJid == null) {
			if (other.buddyJid != null)
				return false;
		} else if (!buddyJid.equals(other.buddyJid))
			return false;
		if (buddyNick == null) {
			if (other.buddyNick != null)
				return false;
		} else if (!buddyNick.equals(other.buddyNick))
			return false;
		return true;
	}
	
	   
}
