package pl.alemac.vikimg.archive;

import java.util.ArrayList;

import pl.alemac.vikimg.R;
import pl.alemac.vikimg.archive.model.BuddyArchiveBinder;
import pl.alemac.vikimg.database.helper.VikimgDatabaseHelper;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ArchiveExpandableListAdapter extends BaseExpandableListAdapter {

	private LayoutInflater inflater;
	private ArrayList<BuddyArchiveBinder> buddyArchiveBinderList;
	private VikimgDatabaseHelper dbHelper;
	private Context context;

	public ArchiveExpandableListAdapter(Context context, ArrayList<BuddyArchiveBinder> buddyArchiveBinderList) {
		this.buddyArchiveBinderList = buddyArchiveBinderList;
		this.context = context;
		dbHelper = new VikimgDatabaseHelper(context);
		inflater = LayoutInflater.from(context);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return buddyArchiveBinderList.get(groupPosition).getArchiveDate().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.archive_child_row, parent, false);
		}

		TextView dateView = (TextView) convertView.findViewById(R.id.archive_date_item);
		String date = buddyArchiveBinderList.get(groupPosition).getArchiveDate().get(childPosition).substring(0, 10);
		dateView.setText(date);
		convertView.setTag(date);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return buddyArchiveBinderList.get(groupPosition).getArchiveDate().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return buddyArchiveBinderList.get(groupPosition).getBuddyNick();
	}

	@Override
	public int getGroupCount() {
		return buddyArchiveBinderList.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.archive_parent_row, parent, false);
		}

		TextView groupName = (TextView) convertView.findViewById(R.id.archive_group_name);
		String group = buddyArchiveBinderList.get(groupPosition).getBuddyNick() + " ("
				+ buddyArchiveBinderList.get(groupPosition).getBuddyJid() + ")";
		groupName.setText(group);
		convertView.setTag(group);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		super.registerDataSetObserver(observer);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
		new ArchiveBuddyTask(this, groupPosition).execute();

	}

	private class ArchiveBuddyTask extends AsyncTask<String, String, String> {

		private ProgressDialog dialog;
		private int groupPosition;
		private ArchiveExpandableListAdapter archiveExpandableListAdapter;

		public ArchiveBuddyTask(ArchiveExpandableListAdapter archiveExpandableListAdapter, int groupPosition) {
			dialog = new ProgressDialog(context);
			this.groupPosition = groupPosition;
			this.archiveExpandableListAdapter = archiveExpandableListAdapter;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage(context.getResources().getString(R.string.data_loading));
			dialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {

			if (buddyArchiveBinderList.get(groupPosition).getArchiveDate().isEmpty()) {
				buddyArchiveBinderList.get(groupPosition).setArchiveDate(
						dbHelper.getDistinctBuddyDates(buddyArchiveBinderList.get(groupPosition).getBuddyJid()));
						dbHelper.closeDB();
			}
			return null;

		}

		@Override
		protected void onPostExecute(String results) {
			super.onPostExecute(results);
			dialog.dismiss();
			archiveExpandableListAdapter.notifyDataSetInvalidated();
		}

	}

}
